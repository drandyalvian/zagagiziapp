package com.l200150089.creiva.zagagizi;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class SuccesAddMakananAct extends AppCompatActivity {

    Button btn_lihatmakanan, btn_addlagi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_succes_addmakanan);

        btn_addlagi =findViewById(R.id.btn_addlagi);
        btn_lihatmakanan =findViewById(R.id.btn_lihatmakanan);

        btn_addlagi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(SuccesAddMakananAct.this,DataMakananAct.class);
                startActivity(go);

            }
        });

        btn_lihatmakanan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // setting timer untuk 1 detik
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // merubah activity ke activity lain
                        Intent go = new Intent(SuccesAddMakananAct.this,PilihMakananAct.class);
                        startActivity(go);
                        finish();
                    }
                }, 1000); // 1000 ms = 1s


            }
        });
    }
}
