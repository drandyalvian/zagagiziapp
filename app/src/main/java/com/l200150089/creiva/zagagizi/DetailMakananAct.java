package com.l200150089.creiva.zagagizi;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ScaleDrawable;
import android.net.ConnectivityManager;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DecimalFormat;
import java.util.Locale;
import java.util.Timer;

public class DetailMakananAct extends AppCompatActivity {

    Button btn_profilunclick;
    Button btn_infounclick;
    Button btn_makanunclick;
    Button btn_tentangunclick;

    ImageView hgram,takaran;

    Button btn_tambah, btn_plusporsi, btn_minporsi, btn_plusgram, btn_mingram;
    TextView textjumlahgram, textjumlahporsi, textjumlahkalori,textjumlahkarbohidrat,
            textjumlahprotein, textjumlahlemak, xnama_makanan, xjenis_makanan;
    Integer valuejumlahgram = 100;
    Integer dbjumlahgram = 0;
    Integer valuejumlahporsi = 1;
    double dbjumlahkalori, dbjumlahkarbohidrat, dbjumlahprotein, dbjumlahlemak = 0;
    double valuejumlahkalori, valuejumlahkarbohidrat, valuejumlahprotein, valuejumlahlemak  = 0;
    double valuetotalkalori, valuetotalkarbohidrat, valuetotalprotein, valuetotallemak = 0;

    DatabaseReference reference, reference3;
    String USERNAME_KEY = "usernamekey";
    String username_key ="";
    String username_key_new ="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_makanan);

        getUsernameLocal();

        takaran = findViewById(R.id.takaran);

        btn_tambah = findViewById(R.id.btn_tambah);
        btn_plusporsi = findViewById(R.id.btn_plusporsi);
        btn_minporsi = findViewById(R.id.btn_minporsi);
        btn_plusgram = findViewById(R.id.btn_plusgram);
        btn_mingram = findViewById(R.id.btn_mingram);

        xnama_makanan = findViewById(R.id.xnama_makanan);
        xjenis_makanan = findViewById(R.id.xjenis_makanan);
        textjumlahgram = findViewById(R.id.textjumlahgram);
        textjumlahporsi = findViewById(R.id.textjumlahporsi);
        textjumlahkalori = findViewById(R.id.textjumlahkalori);
        textjumlahkarbohidrat = findViewById(R.id.textjumlahkarbohidrat);
        textjumlahprotein= findViewById(R.id.textjumlahprotein);
        textjumlahlemak = findViewById(R.id.textjumlahlemak);

//      Setting value baru
        textjumlahporsi.setText(valuejumlahporsi.toString());
        textjumlahgram.setText(valuejumlahgram.toString());

//        textjumlahkalori.setText(valuetotalkalori+"");
//        textjumlahkarbohidrat.setText(valuetotalkarbohidrat+"");
//        textjumlahprotein.setText(valuetotalprotein+"");
//        textjumlahlemak.setText(valuetotallemak+"");

//        new DecimalFormat("##.00").format(valuetotalkalori)

        takaran.animate().alpha(0).setDuration(3000).start();
        takaran.setEnabled(false);





////      awal sembunyikan btn_minporsi
        btn_minporsi.animate().alpha(0).setDuration(300).start();
        btn_minporsi.setEnabled(false);

        btn_plusporsi.animate().alpha(0).setDuration(300).start();
        btn_plusporsi.setEnabled(false);
//
//        btn_mingram.animate().alpha(1).setDuration(300).start();
//        btn_mingram.setEnabled(false);




//      mengambil data dari intent
        Bundle bundle = getIntent().getExtras();
        final String nama_makanan_baru = bundle.getString("key");

//      Mengambil data dari firebase
        reference = FirebaseDatabase.getInstance().getReference().child("DataMakanan").child(nama_makanan_baru);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                xnama_makanan.setText(dataSnapshot.child("nama_makanan").getValue().toString());
                xjenis_makanan.setText(dataSnapshot.child("jenis_makanan").getValue().toString());

                dbjumlahgram = Integer.valueOf(dataSnapshot.child("gram").getValue().toString());

                dbjumlahkalori = Double.valueOf(dataSnapshot.child("kalori").getValue().toString());
                valuetotalkalori = (dbjumlahkalori/dbjumlahgram)*valuejumlahgram;
                textjumlahkalori.setText(String.format(Locale.US,"%.2f",valuetotalkalori));
                dbjumlahkarbohidrat = Double.valueOf(dataSnapshot.child("karbohidrat").getValue().toString());
                valuetotalkarbohidrat = (dbjumlahkarbohidrat/dbjumlahgram)*valuejumlahgram;
                textjumlahkarbohidrat.setText(String.format(Locale.US,"%.2f",valuetotalkarbohidrat));
                dbjumlahprotein = Double.valueOf(dataSnapshot.child("protein").getValue().toString());
                valuetotalprotein = (dbjumlahprotein/dbjumlahgram)*valuejumlahgram;
                textjumlahprotein.setText(String.format(Locale.US,"%.2f",valuetotalprotein));
                dbjumlahlemak = Double.valueOf(dataSnapshot.child("lemak").getValue().toString());
                valuetotallemak = (dbjumlahlemak/dbjumlahgram)*valuejumlahgram;
                textjumlahlemak.setText(String.format(Locale.US,"%.2f",valuetotallemak));

//
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

//      kondisi porsi
        btn_plusporsi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                valuejumlahporsi +=1;
                textjumlahporsi.setText(valuejumlahporsi.toString());
                if (valuejumlahporsi > 1){
                    btn_minporsi.animate().alpha(1).setDuration(300).start();
                    btn_minporsi.setEnabled(true);
                }
                valuetotalkalori = valuejumlahkalori * valuejumlahporsi;
                textjumlahkalori.setText(String.format(Locale.US,"%.2f", valuetotalkalori));

                valuetotalkarbohidrat = valuejumlahkarbohidrat * valuejumlahporsi;
                textjumlahkarbohidrat.setText(String.format(Locale.US,"%.2f", valuetotalkarbohidrat));

                valuetotalprotein = valuejumlahprotein * valuejumlahporsi;
                textjumlahprotein.setText(String.format(Locale.US,"%.2f", valuetotalprotein));

                valuetotallemak = valuejumlahlemak * valuejumlahporsi;
                textjumlahlemak.setText(String.format(Locale.US,"%.2f", valuetotallemak));

            }
        });

        btn_minporsi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                valuejumlahporsi -=1;
                textjumlahporsi.setText(valuejumlahporsi.toString());
                if (valuejumlahporsi < 2){
                    btn_minporsi.animate().alpha(0).setDuration(300).start();
                    btn_minporsi.setEnabled(false);
                }
                valuetotalkalori = valuejumlahkalori * valuejumlahporsi;
                textjumlahkalori.setText(String.format(Locale.US,"%.2f",valuetotalkalori));

                valuetotalkarbohidrat = valuejumlahkarbohidrat * valuejumlahporsi;
                textjumlahkarbohidrat.setText(String.format(Locale.US,"%.2f", valuetotalkarbohidrat));

                valuetotalprotein = valuejumlahprotein * valuejumlahporsi;
                textjumlahprotein.setText(String.format(Locale.US,"%.2f", valuetotalprotein));

                valuetotallemak = valuejumlahlemak * valuejumlahporsi;
                textjumlahlemak.setText(String.format(Locale.US,"%.2f", valuetotallemak));

            }
        });

//      kondisi gram
        btn_plusgram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_plusporsi.animate().alpha(1).setDuration(0).start();
                btn_plusporsi.setEnabled(true);
                valuejumlahgram +=1;
                textjumlahgram.setText(valuejumlahgram.toString());
                if (valuejumlahgram > 1){
                    btn_mingram.animate().alpha(1).setDuration(300).start();
                    btn_mingram.setBackgroundResource(R.drawable.min1);
                    btn_mingram.setEnabled(true);
                }
                valuejumlahkalori = (dbjumlahkalori/dbjumlahgram)*valuejumlahgram;
                valuetotalkalori = valuejumlahkalori * valuejumlahporsi;
                textjumlahkalori.setText(String.format(Locale.US,"%.2f", valuetotalkalori));

                valuejumlahkarbohidrat = (dbjumlahkarbohidrat/dbjumlahgram)*valuejumlahgram;
                valuetotalkarbohidrat = valuejumlahkarbohidrat * valuejumlahporsi;
                textjumlahkarbohidrat.setText(String.format(Locale.US,"%.2f", valuetotalkarbohidrat));

                valuejumlahprotein = (dbjumlahprotein/dbjumlahgram)*valuejumlahgram;
                valuetotalprotein = valuejumlahprotein * valuejumlahporsi;
                textjumlahprotein.setText(String.format(Locale.US,"%.2f", valuetotalprotein));

                valuejumlahlemak = (dbjumlahlemak/dbjumlahgram)*valuejumlahgram;
                valuetotallemak = valuejumlahlemak * valuejumlahporsi;
                textjumlahlemak.setText(String.format(Locale.US,"%.2f", valuetotallemak));

            }
        });

        btn_mingram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_plusporsi.animate().alpha(1).setDuration(0).start();
                btn_plusporsi.setEnabled(true);
                valuejumlahgram -=1;
                textjumlahgram.setText(valuejumlahgram.toString());
                if (valuejumlahgram < 2){
                    btn_mingram.animate().alpha(1).setDuration(300).start();
                    btn_mingram.setBackgroundResource(R.drawable.invisiblemin);
                    btn_mingram.setEnabled(false);

                }
                valuejumlahkalori = (dbjumlahkalori/dbjumlahgram)*valuejumlahgram;
                valuetotalkalori = valuejumlahkalori * valuejumlahporsi;
                textjumlahkalori.setText(String.format(Locale.US,"%.2f", valuetotalkalori));

                valuejumlahkarbohidrat = (dbjumlahkarbohidrat/dbjumlahgram)*valuejumlahgram;
                valuetotalkarbohidrat = valuejumlahkarbohidrat * valuejumlahporsi;
                textjumlahkarbohidrat.setText(String.format(Locale.US,"%.2f", valuetotalkarbohidrat));

                valuejumlahprotein = (dbjumlahprotein/dbjumlahgram)*valuejumlahgram;
                valuetotalprotein = valuejumlahprotein * valuejumlahporsi;
                textjumlahprotein.setText(String.format(Locale.US,"%.2f", valuetotalprotein));

                valuejumlahlemak = (dbjumlahlemak/dbjumlahgram)*valuejumlahgram;
                valuetotallemak = valuejumlahlemak * valuejumlahporsi;
                textjumlahlemak.setText(String.format(Locale.US,"%.2f", valuetotallemak));

            }
        });



//      pindah button sukses
        btn_tambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //menyimpan data ke firbase tabel makananku

                btn_tambah.setEnabled(false);
                btn_tambah.setText("Tunggu...");
                Handler handler1 = new Handler();
                handler1.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        reference3 = FirebaseDatabase.getInstance().getReference()
                                .child("Makananku").child(username_key_new).child(xnama_makanan.getText().toString());
                        reference3.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                dataSnapshot.getRef().child("nama_makanan").setValue(xnama_makanan.getText().toString());
                                dataSnapshot.getRef().child("jenis_makanan").setValue(xjenis_makanan.getText().toString());
                                dataSnapshot.getRef().child("porsi").setValue(valuejumlahporsi.toString());
                                dataSnapshot.getRef().child("gram").setValue(valuejumlahgram.toString());
                                dataSnapshot.getRef().child("kalori").setValue(String.format(Locale.US,"%.2f", valuetotalkalori));
                                dataSnapshot.getRef().child("karbohidrat").setValue(String.format(Locale.US,"%.2f", valuetotalkarbohidrat));
                                dataSnapshot.getRef().child("protein").setValue(String.format(Locale.US,"%.2f", valuetotalprotein));
                                dataSnapshot.getRef().child("lemak").setValue(String.format(Locale.US,"%.2f", valuetotallemak));

                            }


                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                        // setting timer untuk 1 detik

                        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                        android.net.NetworkInfo wifi = cm
                                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
                        android.net.NetworkInfo datac = cm
                                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
                        if((wifi != null & datac != null) && (wifi.isConnected()| datac.isConnected())){
                            // merubah activity ke activity lain
//                            Intent go = new Intent(DetailMakananAct.this,SuccesAddMakananAct.class);
//                            startActivity(go);
//                            finish();
                            Toast toast = Toast.makeText(DetailMakananAct.this,
                                    "Berhasil menambahkan makanan", Toast.LENGTH_SHORT );
                            toast.show();
                        }else {
                            Toast toast = Toast.makeText(DetailMakananAct.this,
                                    "Tidak ada koneksi, pastikan koneksi terhubung dan tidak buruk", Toast.LENGTH_LONG);
                            toast.show();
                            btn_tambah.setEnabled(true);
                            btn_tambah.setText("TAMBAH");
                        }
                        finish();
                    }
                }, 1000);




            }


        });

        hgram = findViewById(R.id.hgram);
        hgram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(DetailMakananAct.this,PedomanGiziAct.class);
                startActivity(go);
            }
        });


//pindah bottom navigation
        btn_profilunclick = findViewById(R.id.btn_profilunclick);
        btn_profilunclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(DetailMakananAct.this,ProfilAct.class);
                startActivity(go);
            }
        });

        btn_makanunclick = findViewById(R.id.btn_makanunclick );
        btn_makanunclick .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(DetailMakananAct.this,PilihMakananAct.class);
                startActivity(go);
            }
        });

        btn_infounclick = findViewById(R.id.btn_infounclick);
        btn_infounclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(DetailMakananAct.this,InfoGiziAct.class);
                startActivity(go);
            }
        });

        btn_tentangunclick = findViewById(R.id.btn_tentangunclick);
        btn_tentangunclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(DetailMakananAct.this,TentangAct.class);
                startActivity(go);
            }
        });
    }
    public void getUsernameLocal(){
        SharedPreferences sharedPreferences = getSharedPreferences(USERNAME_KEY, MODE_PRIVATE);
        username_key_new =sharedPreferences.getString(username_key, "");

    }
}
