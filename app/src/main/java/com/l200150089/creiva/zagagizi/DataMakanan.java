package com.l200150089.creiva.zagagizi;

public class DataMakanan {
    String nama_makanan, jenis_makanan, key;

    public DataMakanan() {
    }

    public DataMakanan(String nama_makanan, String jenis_makanan,String key ) {
        this.nama_makanan = nama_makanan;
        this.jenis_makanan = jenis_makanan;
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getNama_makanan() {
        return nama_makanan;
    }

    public void setNama_makanan(String nama_makanan) {
        this.nama_makanan = nama_makanan;
    }

    public String getJenis_makanan() {
        return jenis_makanan;
    }

    public void setJenis_makanan(String jenis_makanan) {
        this.jenis_makanan = jenis_makanan;
    }
}
