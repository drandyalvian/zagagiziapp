package com.l200150089.creiva.zagagizi;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class DataMakananAct extends AppCompatActivity {

    Button btn_profilunclick;
    Button btn_infounclick;
    Button btn_makanunclick;
    Button btn_tentangunclick;

    Button searchbtn;
    TextView faz, fkacang, fkonf, fumbi,fserialia,fsayuran,fbuah,fdaging,fikan,ftelur,fsusu,fminyak,fminuman;
    Button fbaz, fbkacang, fbkonf, fbumbi,fbserialia,fbsayuran,fbbuah,fbdaging,fbikan,fbtelur,fbsusu,fbminyak,fbminuman;

    DatabaseReference reference, reference2;

    RecyclerView datamakanan_place;
    EditText txtsearch;
//    SearchView txtsearch;
    ArrayList<DataMakanan> list2;
    DataMakananAdapter dataMakananAdapter;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_makanan);

        //filter -----------------------------------
        faz = findViewById(R.id.faz);
        fkacang = findViewById(R.id.fkacang);       fdaging = findViewById(R.id.fdaging);
        fkonf = findViewById(R.id.fkonf);           fikan = findViewById(R.id.fikan);
        fumbi = findViewById(R.id.fumbi);           ftelur = findViewById(R.id.ftelur);
        fserialia = findViewById(R.id.fserialia);   fsusu = findViewById(R.id.fsusu);
        fsayuran = findViewById(R.id.fsayuran);     fminyak = findViewById(R.id.fminyak);
        fbuah = findViewById(R.id.fbuah);           fminuman = findViewById(R.id.fminuman);

        fbaz = findViewById(R.id.fbaz);
        fbkacang = findViewById(R.id.fbkacang);       fbdaging = findViewById(R.id.fbdaging);
        fbkonf = findViewById(R.id.fbkonf);           fbikan = findViewById(R.id.fbikan);
        fbumbi = findViewById(R.id.fbumbi);           fbtelur = findViewById(R.id.fbtelur);
        fbserialia = findViewById(R.id.fbserialia);   fbsusu = findViewById(R.id.fbsusu);
        fbsayuran = findViewById(R.id.fbsayuran);     fbminyak = findViewById(R.id.fbminyak);
        fbbuah = findViewById(R.id.fbbuah);           fbminuman = findViewById(R.id.fbminuman);
        //-------------------------------------

        datamakanan_place = findViewById(R.id.datamakanan_place);
        txtsearch = findViewById(R.id.txtsearch);
        searchbtn = findViewById(R.id.searchbtn);
        datamakanan_place.setHasFixedSize(true);
        datamakanan_place.setLayoutManager(new LinearLayoutManager(this));
        list2 = new ArrayList<DataMakanan>();

        EditText editor = new EditText(this);
        editor.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS);

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.NetworkInfo wifi = cm
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        android.net.NetworkInfo datac = cm
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if((wifi != null & datac != null) && (wifi.isConnected()| datac.isConnected())){
            Toast toast = Toast.makeText(DataMakananAct.this,
                    "Pilih makanan anda", Toast.LENGTH_SHORT);
            toast.show();
        }else {
            Toast toast = Toast.makeText(DataMakananAct.this,
                    "Tidak ada koneksi", Toast.LENGTH_SHORT);
            toast.show();
        }


        reference2 = FirebaseDatabase.getInstance().getReference("DataMakanan");
        reference = FirebaseDatabase.getInstance()
                .getReference().child("DataMakanan");
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot dataSnapshot1: dataSnapshot.getChildren()){
                    DataMakanan p =dataSnapshot1.getValue(DataMakanan.class);
                    list2.add(p);

                    dataMakananAdapter = new DataMakananAdapter(DataMakananAct.this, list2);
                    datamakanan_place.setAdapter(dataMakananAdapter);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }

        });

        fdaging.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filter("Daging");
                fbdaging.setBackgroundResource(R.drawable.downfill);
                fbaz.setBackgroundResource(R.drawable.downfillw);
                fbkacang.setBackgroundResource(R.drawable.downfillw);
                fbkonf.setBackgroundResource(R.drawable.downfillw);
                fbumbi.setBackgroundResource(R.drawable.downfillw);
                fbserialia.setBackgroundResource(R.drawable.downfillw);
                fbsayuran.setBackgroundResource(R.drawable.downfillw);
                fbbuah.setBackgroundResource(R.drawable.downfillw);
                fbminyak.setBackgroundResource(R.drawable.downfillw);
                fbikan.setBackgroundResource(R.drawable.downfillw);
                fbtelur.setBackgroundResource(R.drawable.downfillw);
                fbsusu.setBackgroundResource(R.drawable.downfillw);
                fbminuman.setBackgroundResource(R.drawable.downfillw);



            }
        });
        fbdaging.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filter("Daging");
                fbdaging.setBackgroundResource(R.drawable.downfill);
                fbaz.setBackgroundResource(R.drawable.downfillw);
                fbkacang.setBackgroundResource(R.drawable.downfillw);
                fbkonf.setBackgroundResource(R.drawable.downfillw);
                fbumbi.setBackgroundResource(R.drawable.downfillw);
                fbserialia.setBackgroundResource(R.drawable.downfillw);
                fbsayuran.setBackgroundResource(R.drawable.downfillw);
                fbbuah.setBackgroundResource(R.drawable.downfillw);
                fbminyak.setBackgroundResource(R.drawable.downfillw);
                fbikan.setBackgroundResource(R.drawable.downfillw);
                fbtelur.setBackgroundResource(R.drawable.downfillw);
                fbsusu.setBackgroundResource(R.drawable.downfillw);
                fbminuman.setBackgroundResource(R.drawable.downfillw);

            }
        });

        faz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fbdaging.setBackgroundResource(R.drawable.downfillw);
                fbaz.setBackgroundResource(R.drawable.downfill);
                fbkacang.setBackgroundResource(R.drawable.downfillw);
                fbkonf.setBackgroundResource(R.drawable.downfillw);
                fbumbi.setBackgroundResource(R.drawable.downfillw);
                fbserialia.setBackgroundResource(R.drawable.downfillw);
                fbsayuran.setBackgroundResource(R.drawable.downfillw);
                fbbuah.setBackgroundResource(R.drawable.downfillw);
                fbminyak.setBackgroundResource(R.drawable.downfillw);
                fbikan.setBackgroundResource(R.drawable.downfillw);
                fbtelur.setBackgroundResource(R.drawable.downfillw);
                fbsusu.setBackgroundResource(R.drawable.downfillw);
                fbminuman.setBackgroundResource(R.drawable.downfillw);

                reference.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(dataSnapshot.hasChildren()){
                            list2.clear();
                            for (DataSnapshot dss: dataSnapshot.getChildren()){
                                final DataMakanan filterku = dss.getValue(DataMakanan.class);
                                list2.add(filterku);
                            }

                            DataMakananAdapter dataMakananAdapter = new DataMakananAdapter(DataMakananAct.this,list2);
                            datamakanan_place.setAdapter(dataMakananAdapter);
                            dataMakananAdapter.notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }

                });



            }
        });
        fbaz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fbdaging.setBackgroundResource(R.drawable.downfillw);
                fbaz.setBackgroundResource(R.drawable.downfill);
                fbkacang.setBackgroundResource(R.drawable.downfillw);
                fbkonf.setBackgroundResource(R.drawable.downfillw);
                fbumbi.setBackgroundResource(R.drawable.downfillw);
                fbserialia.setBackgroundResource(R.drawable.downfillw);
                fbsayuran.setBackgroundResource(R.drawable.downfillw);
                fbbuah.setBackgroundResource(R.drawable.downfillw);
                fbminyak.setBackgroundResource(R.drawable.downfillw);
                fbikan.setBackgroundResource(R.drawable.downfillw);
                fbtelur.setBackgroundResource(R.drawable.downfillw);
                fbsusu.setBackgroundResource(R.drawable.downfillw);
                fbminuman.setBackgroundResource(R.drawable.downfillw);

                reference.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(dataSnapshot.hasChildren()){
                            list2.clear();
                            for (DataSnapshot dss: dataSnapshot.getChildren()){
                                final DataMakanan filterku = dss.getValue(DataMakanan.class);
                                list2.add(filterku);
                            }

                            DataMakananAdapter dataMakananAdapter = new DataMakananAdapter(DataMakananAct.this,list2);
                            datamakanan_place.setAdapter(dataMakananAdapter);
                            dataMakananAdapter.notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }

                });

            }
        });

        fkacang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filter("Kacang-kacangan");
                fbdaging.setBackgroundResource(R.drawable.downfillw);
                fbaz.setBackgroundResource(R.drawable.downfillw);
                fbkacang.setBackgroundResource(R.drawable.downfill);
                fbkonf.setBackgroundResource(R.drawable.downfillw);
                fbumbi.setBackgroundResource(R.drawable.downfillw);
                fbserialia.setBackgroundResource(R.drawable.downfillw);
                fbsayuran.setBackgroundResource(R.drawable.downfillw);
                fbbuah.setBackgroundResource(R.drawable.downfillw);
                fbminyak.setBackgroundResource(R.drawable.downfillw);
                fbikan.setBackgroundResource(R.drawable.downfillw);
                fbtelur.setBackgroundResource(R.drawable.downfillw);
                fbsusu.setBackgroundResource(R.drawable.downfillw);
                fbminuman.setBackgroundResource(R.drawable.downfillw);



            }
        });
        fbkacang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filter("Kacang-kacangan");
                fbdaging.setBackgroundResource(R.drawable.downfillw);
                fbaz.setBackgroundResource(R.drawable.downfillw);
                fbkacang.setBackgroundResource(R.drawable.downfill);
                fbkonf.setBackgroundResource(R.drawable.downfillw);
                fbumbi.setBackgroundResource(R.drawable.downfillw);
                fbserialia.setBackgroundResource(R.drawable.downfillw);
                fbsayuran.setBackgroundResource(R.drawable.downfillw);
                fbbuah.setBackgroundResource(R.drawable.downfillw);
                fbminyak.setBackgroundResource(R.drawable.downfillw);
                fbikan.setBackgroundResource(R.drawable.downfillw);
                fbtelur.setBackgroundResource(R.drawable.downfillw);
                fbsusu.setBackgroundResource(R.drawable.downfillw);
                fbminuman.setBackgroundResource(R.drawable.downfillw);

            }
        });

        fkonf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filter("Konfeksioneri");
                fbdaging.setBackgroundResource(R.drawable.downfillw);
                fbaz.setBackgroundResource(R.drawable.downfillw);
                fbkacang.setBackgroundResource(R.drawable.downfillw);
                fbkonf.setBackgroundResource(R.drawable.downfill);
                fbumbi.setBackgroundResource(R.drawable.downfillw);
                fbserialia.setBackgroundResource(R.drawable.downfillw);
                fbsayuran.setBackgroundResource(R.drawable.downfillw);
                fbbuah.setBackgroundResource(R.drawable.downfillw);
                fbminyak.setBackgroundResource(R.drawable.downfillw);
                fbikan.setBackgroundResource(R.drawable.downfillw);
                fbtelur.setBackgroundResource(R.drawable.downfillw);
                fbsusu.setBackgroundResource(R.drawable.downfillw);
                fbminuman.setBackgroundResource(R.drawable.downfillw);



            }
        });
        fbkonf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filter("Konfeksioneri");
                fbdaging.setBackgroundResource(R.drawable.downfillw);
                fbaz.setBackgroundResource(R.drawable.downfillw);
                fbkacang.setBackgroundResource(R.drawable.downfillw);
                fbkonf.setBackgroundResource(R.drawable.downfill);
                fbumbi.setBackgroundResource(R.drawable.downfillw);
                fbserialia.setBackgroundResource(R.drawable.downfillw);
                fbsayuran.setBackgroundResource(R.drawable.downfillw);
                fbbuah.setBackgroundResource(R.drawable.downfillw);
                fbminyak.setBackgroundResource(R.drawable.downfillw);
                fbikan.setBackgroundResource(R.drawable.downfillw);
                fbtelur.setBackgroundResource(R.drawable.downfillw);
                fbsusu.setBackgroundResource(R.drawable.downfillw);
                fbminuman.setBackgroundResource(R.drawable.downfillw);

            }
        });

        fumbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filter("Umbi");
                fbdaging.setBackgroundResource(R.drawable.downfillw);
                fbaz.setBackgroundResource(R.drawable.downfillw);
                fbkacang.setBackgroundResource(R.drawable.downfillw);
                fbkonf.setBackgroundResource(R.drawable.downfillw);
                fbumbi.setBackgroundResource(R.drawable.downfill);
                fbserialia.setBackgroundResource(R.drawable.downfillw);
                fbsayuran.setBackgroundResource(R.drawable.downfillw);
                fbbuah.setBackgroundResource(R.drawable.downfillw);
                fbminyak.setBackgroundResource(R.drawable.downfillw);
                fbikan.setBackgroundResource(R.drawable.downfillw);
                fbtelur.setBackgroundResource(R.drawable.downfillw);
                fbsusu.setBackgroundResource(R.drawable.downfillw);
                fbminuman.setBackgroundResource(R.drawable.downfillw);



            }
        });
        fbumbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filter("Umbi");
                fbdaging.setBackgroundResource(R.drawable.downfillw);
                fbaz.setBackgroundResource(R.drawable.downfillw);
                fbkacang.setBackgroundResource(R.drawable.downfillw);
                fbkonf.setBackgroundResource(R.drawable.downfillw);
                fbumbi.setBackgroundResource(R.drawable.downfill);
                fbserialia.setBackgroundResource(R.drawable.downfillw);
                fbsayuran.setBackgroundResource(R.drawable.downfillw);
                fbbuah.setBackgroundResource(R.drawable.downfillw);
                fbminyak.setBackgroundResource(R.drawable.downfillw);
                fbikan.setBackgroundResource(R.drawable.downfillw);
                fbtelur.setBackgroundResource(R.drawable.downfillw);
                fbsusu.setBackgroundResource(R.drawable.downfillw);
                fbminuman.setBackgroundResource(R.drawable.downfillw);

            }
        });

        fserialia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filter("Serialia");
                fbdaging.setBackgroundResource(R.drawable.downfillw);
                fbaz.setBackgroundResource(R.drawable.downfillw);
                fbkacang.setBackgroundResource(R.drawable.downfillw);
                fbkonf.setBackgroundResource(R.drawable.downfillw);
                fbumbi.setBackgroundResource(R.drawable.downfillw);
                fbserialia.setBackgroundResource(R.drawable.downfill);
                fbsayuran.setBackgroundResource(R.drawable.downfillw);
                fbbuah.setBackgroundResource(R.drawable.downfillw);
                fbminyak.setBackgroundResource(R.drawable.downfillw);
                fbikan.setBackgroundResource(R.drawable.downfillw);
                fbtelur.setBackgroundResource(R.drawable.downfillw);
                fbsusu.setBackgroundResource(R.drawable.downfillw);
                fbminuman.setBackgroundResource(R.drawable.downfillw);



            }
        });
        fserialia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filter("Serialia");
                fbdaging.setBackgroundResource(R.drawable.downfillw);
                fbaz.setBackgroundResource(R.drawable.downfillw);
                fbkacang.setBackgroundResource(R.drawable.downfillw);
                fbkonf.setBackgroundResource(R.drawable.downfillw);
                fbumbi.setBackgroundResource(R.drawable.downfillw);
                fbserialia.setBackgroundResource(R.drawable.downfill);
                fbsayuran.setBackgroundResource(R.drawable.downfillw);
                fbbuah.setBackgroundResource(R.drawable.downfillw);
                fbminyak.setBackgroundResource(R.drawable.downfillw);
                fbikan.setBackgroundResource(R.drawable.downfillw);
                fbtelur.setBackgroundResource(R.drawable.downfillw);
                fbsusu.setBackgroundResource(R.drawable.downfillw);
                fbminuman.setBackgroundResource(R.drawable.downfillw);

            }
        });
        fsayuran.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filter("Sayuran");
                fbdaging.setBackgroundResource(R.drawable.downfillw);
                fbaz.setBackgroundResource(R.drawable.downfillw);
                fbkacang.setBackgroundResource(R.drawable.downfillw);
                fbkonf.setBackgroundResource(R.drawable.downfillw);
                fbumbi.setBackgroundResource(R.drawable.downfillw);
                fbserialia.setBackgroundResource(R.drawable.downfillw);
                fbsayuran.setBackgroundResource(R.drawable.downfill);
                fbbuah.setBackgroundResource(R.drawable.downfillw);
                fbminyak.setBackgroundResource(R.drawable.downfillw);
                fbikan.setBackgroundResource(R.drawable.downfillw);
                fbtelur.setBackgroundResource(R.drawable.downfillw);
                fbsusu.setBackgroundResource(R.drawable.downfillw);
                fbminuman.setBackgroundResource(R.drawable.downfillw);



            }
        });
        fsayuran.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filter("Sayuran");
                fbdaging.setBackgroundResource(R.drawable.downfillw);
                fbaz.setBackgroundResource(R.drawable.downfillw);
                fbkacang.setBackgroundResource(R.drawable.downfillw);
                fbkonf.setBackgroundResource(R.drawable.downfillw);
                fbumbi.setBackgroundResource(R.drawable.downfillw);
                fbserialia.setBackgroundResource(R.drawable.downfillw);
                fbsayuran.setBackgroundResource(R.drawable.downfill);
                fbbuah.setBackgroundResource(R.drawable.downfillw);
                fbminyak.setBackgroundResource(R.drawable.downfillw);
                fbikan.setBackgroundResource(R.drawable.downfillw);
                fbtelur.setBackgroundResource(R.drawable.downfillw);
                fbsusu.setBackgroundResource(R.drawable.downfillw);
                fbminuman.setBackgroundResource(R.drawable.downfillw);

            }
        });

        fbuah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filter("Buah");
                fbdaging.setBackgroundResource(R.drawable.downfillw);
                fbaz.setBackgroundResource(R.drawable.downfillw);
                fbkacang.setBackgroundResource(R.drawable.downfillw);
                fbkonf.setBackgroundResource(R.drawable.downfillw);
                fbumbi.setBackgroundResource(R.drawable.downfillw);
                fbserialia.setBackgroundResource(R.drawable.downfillw);
                fbsayuran.setBackgroundResource(R.drawable.downfillw);
                fbbuah.setBackgroundResource(R.drawable.downfill);
                fbminyak.setBackgroundResource(R.drawable.downfillw);
                fbikan.setBackgroundResource(R.drawable.downfillw);
                fbtelur.setBackgroundResource(R.drawable.downfillw);
                fbsusu.setBackgroundResource(R.drawable.downfillw);
                fbminuman.setBackgroundResource(R.drawable.downfillw);



            }
        });
        fbuah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filter("Buah");
                fbdaging.setBackgroundResource(R.drawable.downfillw);
                fbaz.setBackgroundResource(R.drawable.downfillw);
                fbkacang.setBackgroundResource(R.drawable.downfillw);
                fbkonf.setBackgroundResource(R.drawable.downfillw);
                fbumbi.setBackgroundResource(R.drawable.downfillw);
                fbserialia.setBackgroundResource(R.drawable.downfillw);
                fbsayuran.setBackgroundResource(R.drawable.downfillw);
                fbbuah.setBackgroundResource(R.drawable.downfill);
                fbminyak.setBackgroundResource(R.drawable.downfillw);
                fbikan.setBackgroundResource(R.drawable.downfillw);
                fbtelur.setBackgroundResource(R.drawable.downfillw);
                fbsusu.setBackgroundResource(R.drawable.downfillw);
                fbminuman.setBackgroundResource(R.drawable.downfillw);

            }
        });
        fminyak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filter("Minyak");
                fbdaging.setBackgroundResource(R.drawable.downfillw);
                fbaz.setBackgroundResource(R.drawable.downfillw);
                fbkacang.setBackgroundResource(R.drawable.downfillw);
                fbkonf.setBackgroundResource(R.drawable.downfillw);
                fbumbi.setBackgroundResource(R.drawable.downfillw);
                fbserialia.setBackgroundResource(R.drawable.downfillw);
                fbsayuran.setBackgroundResource(R.drawable.downfillw);
                fbbuah.setBackgroundResource(R.drawable.downfillw);
                fbminyak.setBackgroundResource(R.drawable.downfill);
                fbikan.setBackgroundResource(R.drawable.downfillw);
                fbtelur.setBackgroundResource(R.drawable.downfillw);
                fbsusu.setBackgroundResource(R.drawable.downfillw);
                fbminuman.setBackgroundResource(R.drawable.downfillw);



            }
        });
        fbminyak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filter("Minyak");
                fbdaging.setBackgroundResource(R.drawable.downfillw);
                fbaz.setBackgroundResource(R.drawable.downfillw);
                fbkacang.setBackgroundResource(R.drawable.downfillw);
                fbkonf.setBackgroundResource(R.drawable.downfillw);
                fbumbi.setBackgroundResource(R.drawable.downfillw);
                fbserialia.setBackgroundResource(R.drawable.downfillw);
                fbsayuran.setBackgroundResource(R.drawable.downfillw);
                fbbuah.setBackgroundResource(R.drawable.downfillw);
                fbminyak.setBackgroundResource(R.drawable.downfill);
                fbikan.setBackgroundResource(R.drawable.downfillw);
                fbtelur.setBackgroundResource(R.drawable.downfillw);
                fbsusu.setBackgroundResource(R.drawable.downfillw);
                fbminuman.setBackgroundResource(R.drawable.downfillw);

            }
        });

        fikan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filter("Ikan");
                fbdaging.setBackgroundResource(R.drawable.downfillw);
                fbaz.setBackgroundResource(R.drawable.downfillw);
                fbkacang.setBackgroundResource(R.drawable.downfillw);
                fbkonf.setBackgroundResource(R.drawable.downfillw);
                fbumbi.setBackgroundResource(R.drawable.downfillw);
                fbserialia.setBackgroundResource(R.drawable.downfillw);
                fbsayuran.setBackgroundResource(R.drawable.downfillw);
                fbbuah.setBackgroundResource(R.drawable.downfillw);
                fbminyak.setBackgroundResource(R.drawable.downfillw);
                fbikan.setBackgroundResource(R.drawable.downfill);
                fbtelur.setBackgroundResource(R.drawable.downfillw);
                fbsusu.setBackgroundResource(R.drawable.downfillw);
                fbminuman.setBackgroundResource(R.drawable.downfillw);


            }
        });
        fbikan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filter("Ikan");
                fbdaging.setBackgroundResource(R.drawable.downfillw);
                fbaz.setBackgroundResource(R.drawable.downfillw);
                fbkacang.setBackgroundResource(R.drawable.downfillw);
                fbkonf.setBackgroundResource(R.drawable.downfillw);
                fbumbi.setBackgroundResource(R.drawable.downfillw);
                fbserialia.setBackgroundResource(R.drawable.downfillw);
                fbsayuran.setBackgroundResource(R.drawable.downfillw);
                fbbuah.setBackgroundResource(R.drawable.downfillw);
                fbminyak.setBackgroundResource(R.drawable.downfillw);
                fbikan.setBackgroundResource(R.drawable.downfill);
                fbtelur.setBackgroundResource(R.drawable.downfillw);
                fbsusu.setBackgroundResource(R.drawable.downfillw);
                fbminuman.setBackgroundResource(R.drawable.downfillw);

            }
        });

        ftelur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filter("Telur");
                fbdaging.setBackgroundResource(R.drawable.downfillw);
                fbaz.setBackgroundResource(R.drawable.downfillw);
                fbkacang.setBackgroundResource(R.drawable.downfillw);
                fbkonf.setBackgroundResource(R.drawable.downfillw);
                fbumbi.setBackgroundResource(R.drawable.downfillw);
                fbserialia.setBackgroundResource(R.drawable.downfillw);
                fbsayuran.setBackgroundResource(R.drawable.downfillw);
                fbbuah.setBackgroundResource(R.drawable.downfillw);
                fbminyak.setBackgroundResource(R.drawable.downfillw);
                fbikan.setBackgroundResource(R.drawable.downfillw);
                fbtelur.setBackgroundResource(R.drawable.downfill);
                fbsusu.setBackgroundResource(R.drawable.downfillw);
                fbminuman.setBackgroundResource(R.drawable.downfillw);


            }
        });
        fbtelur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filter("Telur");
                fbdaging.setBackgroundResource(R.drawable.downfillw);
                fbaz.setBackgroundResource(R.drawable.downfillw);
                fbkacang.setBackgroundResource(R.drawable.downfillw);
                fbkonf.setBackgroundResource(R.drawable.downfillw);
                fbumbi.setBackgroundResource(R.drawable.downfillw);
                fbserialia.setBackgroundResource(R.drawable.downfillw);
                fbsayuran.setBackgroundResource(R.drawable.downfillw);
                fbbuah.setBackgroundResource(R.drawable.downfillw);
                fbminyak.setBackgroundResource(R.drawable.downfillw);
                fbikan.setBackgroundResource(R.drawable.downfillw);
                fbtelur.setBackgroundResource(R.drawable.downfill);
                fbsusu.setBackgroundResource(R.drawable.downfillw);
                fbminuman.setBackgroundResource(R.drawable.downfillw);

            }
        });

        fsusu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filter("Susu");
                fbdaging.setBackgroundResource(R.drawable.downfillw);
                fbaz.setBackgroundResource(R.drawable.downfillw);
                fbkacang.setBackgroundResource(R.drawable.downfillw);
                fbkonf.setBackgroundResource(R.drawable.downfillw);
                fbumbi.setBackgroundResource(R.drawable.downfillw);
                fbserialia.setBackgroundResource(R.drawable.downfillw);
                fbsayuran.setBackgroundResource(R.drawable.downfillw);
                fbbuah.setBackgroundResource(R.drawable.downfillw);
                fbminyak.setBackgroundResource(R.drawable.downfillw);
                fbikan.setBackgroundResource(R.drawable.downfillw);
                fbtelur.setBackgroundResource(R.drawable.downfillw);
                fbsusu.setBackgroundResource(R.drawable.downfill);
                fbminuman.setBackgroundResource(R.drawable.downfillw);


            }
        });
        fbsusu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filter("Susu");
                fbdaging.setBackgroundResource(R.drawable.downfillw);
                fbaz.setBackgroundResource(R.drawable.downfillw);
                fbkacang.setBackgroundResource(R.drawable.downfillw);
                fbkonf.setBackgroundResource(R.drawable.downfillw);
                fbumbi.setBackgroundResource(R.drawable.downfillw);
                fbserialia.setBackgroundResource(R.drawable.downfillw);
                fbsayuran.setBackgroundResource(R.drawable.downfillw);
                fbbuah.setBackgroundResource(R.drawable.downfillw);
                fbminyak.setBackgroundResource(R.drawable.downfillw);
                fbikan.setBackgroundResource(R.drawable.downfillw);
                fbtelur.setBackgroundResource(R.drawable.downfillw);
                fbsusu.setBackgroundResource(R.drawable.downfill);
                fbminuman.setBackgroundResource(R.drawable.downfillw);

            }
        });

        fminuman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filter("Minuman");
                fbdaging.setBackgroundResource(R.drawable.downfillw);
                fbaz.setBackgroundResource(R.drawable.downfillw);
                fbkacang.setBackgroundResource(R.drawable.downfillw);
                fbkonf.setBackgroundResource(R.drawable.downfillw);
                fbumbi.setBackgroundResource(R.drawable.downfillw);
                fbserialia.setBackgroundResource(R.drawable.downfillw);
                fbsayuran.setBackgroundResource(R.drawable.downfillw);
                fbbuah.setBackgroundResource(R.drawable.downfillw);
                fbminyak.setBackgroundResource(R.drawable.downfillw);
                fbikan.setBackgroundResource(R.drawable.downfillw);
                fbtelur.setBackgroundResource(R.drawable.downfillw);
                fbsusu.setBackgroundResource(R.drawable.downfillw);
                fbminuman.setBackgroundResource(R.drawable.downfill);


            }
        });
        fbsusu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filter("Minuman");
                fbdaging.setBackgroundResource(R.drawable.downfillw);
                fbaz.setBackgroundResource(R.drawable.downfillw);
                fbkacang.setBackgroundResource(R.drawable.downfillw);
                fbkonf.setBackgroundResource(R.drawable.downfillw);
                fbumbi.setBackgroundResource(R.drawable.downfillw);
                fbserialia.setBackgroundResource(R.drawable.downfillw);
                fbsayuran.setBackgroundResource(R.drawable.downfillw);
                fbbuah.setBackgroundResource(R.drawable.downfillw);
                fbminyak.setBackgroundResource(R.drawable.downfillw);
                fbikan.setBackgroundResource(R.drawable.downfillw);
                fbtelur.setBackgroundResource(R.drawable.downfillw);
                fbsusu.setBackgroundResource(R.drawable.downfillw);
                fbminuman.setBackgroundResource(R.drawable.downfill);

            }
        });




        txtsearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (txtsearch.getText().toString().length() == 1 && txtsearch.getTag().toString().equals("true"))
                {
                    txtsearch.setTag("false");
                    txtsearch.setText(txtsearch.getText().toString().toUpperCase());
                    txtsearch.setSelection(txtsearch.getText().toString().length());
                }
                if(txtsearch.getText().toString().length() == 0)
                {
                    txtsearch.setTag("true");
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().isEmpty()){
                    search(s.toString());
                }else {
                    search("");
                }


            }
        });






//pindah bottom navigation
        btn_profilunclick = findViewById(R.id.btn_profilunclick);
        btn_profilunclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(DataMakananAct.this,ProfilAct.class);
                startActivity(go);
            }
        });

        btn_makanunclick = findViewById(R.id.btn_makanunclick );
        btn_makanunclick .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(DataMakananAct.this,PilihMakananAct.class);
                startActivity(go);
            }
        });

        btn_infounclick = findViewById(R.id.btn_infounclick);
        btn_infounclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(DataMakananAct.this,InfoGiziAct.class);
                startActivity(go);
            }
        });

        btn_tentangunclick = findViewById(R.id.btn_tentangunclick);
        btn_tentangunclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(DataMakananAct.this,TentangAct.class);
                startActivity(go);
            }
        });



    }

    private void search(String s) {
        Query query = reference.orderByChild("nama_makanan")
                .startAt(s)
                .endAt(s+"\uf8ff");
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChildren()){
                    list2.clear();
                    for (DataSnapshot dss: dataSnapshot.getChildren()){
                        final DataMakanan dataMakanan = dss.getValue(DataMakanan.class);
                        list2.add(dataMakanan);
                    }

                    DataMakananAdapter dataMakananAdapter = new DataMakananAdapter(DataMakananAct.this,list2);
                    datamakanan_place.setAdapter(dataMakananAdapter);
                    dataMakananAdapter.notifyDataSetChanged();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    private void filter (String d){

        Query query =reference2.orderByChild("jenis_makanan").equalTo(d);

        query.addListenerForSingleValueEvent (new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChildren()){
                    list2.clear();
                    for (DataSnapshot dss: dataSnapshot.getChildren()){
                        final DataMakanan filterku = dss.getValue(DataMakanan.class);
                        list2.add(filterku);
                    }

                    DataMakananAdapter dataMakananAdapter = new DataMakananAdapter(DataMakananAct.this,list2);
                    datamakanan_place.setAdapter(dataMakananAdapter);
                    dataMakananAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }


}
