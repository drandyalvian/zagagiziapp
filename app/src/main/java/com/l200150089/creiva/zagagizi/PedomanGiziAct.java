package com.l200150089.creiva.zagagizi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class PedomanGiziAct extends AppCompatActivity {

    Button btn_profilunclick;
    Button btn_infounclick;
    Button btn_makanunclick;
    Button btn_tentangunclick;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedoman_gizi);


        // Pindah acivity Bottom Navigation
        btn_profilunclick = findViewById(R.id.btn_profilunclick);
        btn_profilunclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(PedomanGiziAct.this,ProfilAct.class);
                startActivity(go);
            }
        });

        btn_makanunclick = findViewById(R.id.btn_makanunclick );
        btn_makanunclick .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(PedomanGiziAct.this,PilihMakananAct.class);
                startActivity(go);
            }
        });

        btn_infounclick = findViewById(R.id.btn_infounclick);
        btn_infounclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(PedomanGiziAct.this,InfoGiziAct.class);
                startActivity(go);
            }
        });

        btn_tentangunclick = findViewById(R.id.btn_tentangunclick);
        btn_tentangunclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(PedomanGiziAct.this,TentangAct.class);
                startActivity(go);
            }
        });
    }
}
