package com.l200150089.creiva.zagagizi;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.AndroidException;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.List;

public class RegisterTwoAct extends AppCompatActivity {

    //firebase
    EditText nama_lengkap,tinggi_badan, berat_badan;
    TextView usia;
    SeekBar seekbarusia;
    Button infoaktivitas;

    int maxusia;

    DatabaseReference reference;

    String USERNAME_KEY = "usernamekey";
    String username_key ="";
    String username_key_new ="";

    //act
    Button btn_selanjutnya;
    LinearLayout btn_back;
    Spinner spinner,spinner2;

    //ArrayAdapter<CharSequence> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_two);

        EditText editor = new EditText(this);
        editor.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS);

        getUsernameLocal();

        infoaktivitas = findViewById(R.id.infoaktivitas);

        nama_lengkap=findViewById(R.id.nama_lengkap);
        usia=findViewById(R.id.usia);
        seekbarusia=findViewById(R.id.seekbarusia);

        tinggi_badan=findViewById(R.id.tinggi_badan);
        berat_badan=findViewById(R.id.berat_badan);

        nama_lengkap.addTextChangedListener(loginTextWatcher);
        tinggi_badan.addTextChangedListener(loginTextWatcher);
        berat_badan.addTextChangedListener(loginTextWatcher);


        spinner=(Spinner) findViewById(R.id.spinner);
        spinner2=(Spinner) findViewById(R.id.spinner2);

        final ArrayAdapter pilihGender=ArrayAdapter.createFromResource(this, R.array.pilih_gender, android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(pilihGender);

        final ArrayAdapter pilihAktivitas=ArrayAdapter.createFromResource(this, R.array.pilih_aktivitas, android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(pilihAktivitas);


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        seekbarusia.setMax(51);
        seekbarusia.setProgress(0);
        seekbarusia.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progress = progress + 19;
                usia.setText(String.valueOf(progress));

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        infoaktivitas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dialog dialog = new Dialog(RegisterTwoAct.this);
                dialog.setContentView(R.layout.dialog_aktivitas);
                dialog.setTitle("Aktivitas");
                dialog.show();
            }
        });


        btn_selanjutnya=findViewById(R.id.btn_selanjutnya);

        btn_selanjutnya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //loading
                btn_selanjutnya.setEnabled(false);
                btn_selanjutnya.setText("Tunggu...");

                //Simpan Database
                reference = FirebaseDatabase.getInstance().getReference()
                        .child("Users").child(username_key_new);
                reference.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        reference.getRef().child("nama_lengkap").setValue(nama_lengkap.getText().toString());
                        reference.getRef().child("usia").setValue(usia.getText().toString());
                        //reference.getRef().child("aktivitas").setValue(aktivitas.getText().toString());
                        //reference.getRef().child("jenis_kelamin").setValue(jenis_kelamin.getText().toString());
                        reference.getRef().child("tinggi_badan").setValue(tinggi_badan.getText().toString());
                        reference.getRef().child("berat_badan").setValue(berat_badan.getText().toString());
                        reference.getRef().child("jenis_kelamin").setValue(spinner.getSelectedItem().toString());
                        reference.getRef().child("aktivitas").setValue(spinner2.getSelectedItem().toString());




                    }


                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {



                    }
                });

                ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                android.net.NetworkInfo wifi = cm
                        .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
                android.net.NetworkInfo datac = cm
                        .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
                if((wifi != null & datac != null) && (wifi.isConnected()| datac.isConnected())){
                    //pindah activity
                    Intent gotoregistwo = new Intent(RegisterTwoAct.this, SuccesRegisterAct.class);
                    startActivity(gotoregistwo);
                }else {
                    Toast toast = Toast.makeText(RegisterTwoAct.this,
                            "Tidak ada koneksi, pastikan koneksi anda terhubung dan tidak buruk", Toast.LENGTH_LONG);
                    toast.show();
                    btn_selanjutnya.setEnabled(true);
                    btn_selanjutnya.setText("Lanjut");
                }


            }
        });
        btn_back = findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goseeprofil = new Intent(RegisterTwoAct.this,RegisterOneAct.class);
                startActivity(goseeprofil);
            }
        });

    }

    public void getUsernameLocal(){
        SharedPreferences sharedPreferences = getSharedPreferences(USERNAME_KEY, MODE_PRIVATE);
        username_key_new =sharedPreferences.getString(username_key, "");

    }

    private TextWatcher loginTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

            String tbInput = tinggi_badan.getText().toString().trim();
            String bbInput = berat_badan.getText().toString().trim();
            String namaInput = nama_lengkap.getText().toString().trim();

            btn_selanjutnya.setEnabled(!tbInput.isEmpty() && !bbInput.isEmpty() && !namaInput.isEmpty());

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

}
