package com.l200150089.creiva.zagagizi;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class AdminRegiterAct extends AppCompatActivity {

    //act
    Button btn_daftar;
    LinearLayout btn_back;

    //firebase
    EditText username, password, nama_lengkap;
    DatabaseReference reference;

    String USERNAME_KEY = "usernamekey";
    String username_key ="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_regiter);

        username=findViewById(R.id.username);
        password=findViewById(R.id.password);
        nama_lengkap=findViewById(R.id.nama_lengkap);


        btn_daftar=findViewById(R.id.btn_daftar);

        btn_daftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //loading
                btn_daftar.setEnabled(false);
                btn_daftar.setText("Tunggu...");

//                //menyimpan data local storage
//                SharedPreferences sharedPreferences = getSharedPreferences(USERNAME_KEY, MODE_PRIVATE);
//                SharedPreferences.Editor editor = sharedPreferences.edit();
//                editor.putString(username_key, username.getText().toString());
//                editor.apply();

                //Simpan Database
                reference = FirebaseDatabase.getInstance().getReference()
                        .child("Admin").child(username.getText().toString());

                reference.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        dataSnapshot.getRef().child("username").setValue(username.getText().toString());
                        dataSnapshot.getRef().child("password").setValue(password.getText().toString());
                        dataSnapshot.getRef().child("nama_lengkap").setValue(nama_lengkap.getText().toString());
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
                //pindah activity admin menu
                Intent go=new Intent(AdminRegiterAct.this,AdminMenu1.class);
                startActivity(go);
                Toast toast = Toast.makeText(AdminRegiterAct.this,
                        "Berhasil daftar admin baru", Toast.LENGTH_SHORT);
                toast.show();

            }
        });

        btn_back = findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(AdminRegiterAct.this,AdminMenu1.class);
                startActivity(go);
            }
        });


    }
}
