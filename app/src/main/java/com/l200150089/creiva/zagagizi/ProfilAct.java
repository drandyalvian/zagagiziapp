package com.l200150089.creiva.zagagizi;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Locale;

public class ProfilAct extends AppCompatActivity {

    boolean doubleBackToExitPressedOnce = false;

    //firebase
    TextView nama_lengkap;

    DatabaseReference reference;
    String USERNAME_KEY = "usernamekey";
    String username_key = "";
    String username_key_new ="";

    //bottom navigation
    Button btn_makanunclick;
    Button btn_tentangunclick;
    Button btn_infounclick;

    //profil
    ImageView btn_imageView, kondisibb, editp;
    //IMT
    double imt = 0;
    double dbberatbadan = 0;
    double dbtinggibadan = 0;
    //AMB
    Integer dbusia =0;
    String dbgender = "";
    double amb =0;
    //Aktivitas
    String dbaktivitas = "";
    double xaktivitas = 0;

    TextView kebkalori,kebkarbohidrat,kebprotein,keblemak, textimt;
    double jmlkebkalori,jmlkebkarbohidrat1,jmlkebprotein1, jmlkeblemak1,
            jmlkebkarbohidrat2,jmlkebprotein2, jmlkeblemak2 = 0;
    double karbohidrat1 = 0.60;
    double karbohidrat2 = 0.75;
    double protein1 = 0.10;
    double protein2 = 0.15;
    double lemak1 = 0.10;
    double lemak2 = 0.25;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil);

        getUsernameLocal();
        nama_lengkap = findViewById(R.id.nama_lengkap);
        kondisibb = findViewById(R.id.kondisibb);
        kebkalori = findViewById(R.id.kebkalori);
        kebkarbohidrat = findViewById(R.id.kebkarbohidrat);
        kebprotein = findViewById(R.id.kebprotein);
        keblemak = findViewById(R.id.keblemak);

        textimt = findViewById(R.id.textimt);
        editp = findViewById(R.id.editp);

        editp.animate().alpha(0).setDuration(3000).start();
        editp.setEnabled(false);




        reference= FirebaseDatabase.getInstance().getReference().
                child("Users").child(username_key_new);
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                nama_lengkap.setText(dataSnapshot.child("nama_lengkap").getValue().toString());

                //IMT
                dbberatbadan = Integer.valueOf(dataSnapshot.child("berat_badan").getValue().toString());
                dbtinggibadan = Integer.valueOf(dataSnapshot.child("tinggi_badan").getValue().toString());
                imt = dbberatbadan / ((dbtinggibadan/100) *(dbtinggibadan/100));

                textimt.setText(String.format(Locale.US,"%.1f", imt));

                if (imt < 17.0){
                    kondisibb.setBackgroundResource(R.drawable.bbsangatkurus);
                }else if(imt >= 17.0 && imt <= 18.5 ){
                    kondisibb.setBackgroundResource(R.drawable.bbkurus);
                }else if(imt > 18.5 && imt <= 25.0 ) {
                    kondisibb.setBackgroundResource(R.drawable.bbnormal);
                }else if(imt > 25.0 && imt <= 27.0 ) {
                    kondisibb.setBackgroundResource(R.drawable.bbgemuk);
                }else if(imt > 27.0  ) {
                    kondisibb.setBackgroundResource(R.drawable.bbsangatgemuk);
                }else{
                    Toast.makeText(getApplicationContext(),"salah input!", Toast.LENGTH_SHORT).show();
                }

                //AMB
                dbusia = Integer.valueOf(dataSnapshot.child("usia").getValue().toString());
                dbgender = dataSnapshot.child("jenis_kelamin").getValue().toString();
                if (dbgender.equals("Laki - laki")){
                    amb = 66 + (13.7 * dbberatbadan)+(5*dbtinggibadan)-(6.8*dbusia);
                }
                if (dbgender.equals("Perempuan")){
                    amb = 65.5 + (9.6 * dbberatbadan)+(1.8*dbtinggibadan)-(4.7*dbusia);
                }

                //AKTIVITAS
                dbaktivitas = dataSnapshot.child("aktivitas").getValue().toString();

                if (dbgender.equals("Laki - laki") ){
                    if (dbaktivitas.equals("Sangat Ringan")){
                        xaktivitas = 1.30;
                    }else if (dbaktivitas.equals("Ringan")){
                        xaktivitas = 1.65;
                    }else if (dbaktivitas.equals("Sedang")){
                        xaktivitas = 1.76;
                    }else if (dbaktivitas.equals("Berat")){
                        xaktivitas = 2.10;
                    }else {
                        Toast.makeText(getApplicationContext(),"Null!", Toast.LENGTH_SHORT).show();
                    }
                }
                if (dbgender.equals("Perempuan")){
                    if (dbaktivitas.equals("Sangat Ringan") ){
                        xaktivitas = 1.30;
                    }else if (dbaktivitas.equals("Ringan") ){
                        xaktivitas = 1.55;
                    }else if (dbaktivitas.equals("Sedang")){
                        xaktivitas = 1.70;
                    }else if (dbaktivitas.equals("Berat")){
                        xaktivitas = 2.00;
                    }else {
                        Toast.makeText(getApplicationContext(),"Null!", Toast.LENGTH_SHORT).show();
                    }
                }

                //Hitung kebutuhan nutrisi
                if (imt < 18.5 ){
                    jmlkebkalori = (xaktivitas * amb)+500;
                    kebkalori.setText(String.format(Locale.US,"%.2f", jmlkebkalori)+" kkal");
                }
                if (imt > 18.5 && imt <= 25.0 ){
                    jmlkebkalori = xaktivitas * amb;
                    kebkalori.setText(String.format(Locale.US,"%.2f", jmlkebkalori)+" kkal");
                }

                if (imt > 25.0 ){
                    jmlkebkalori = (xaktivitas * amb)- 500;
                    kebkalori.setText(String.format(Locale.US,"%.2f", jmlkebkalori)+" kkal");
                }

                jmlkebkarbohidrat1 = (jmlkebkalori*karbohidrat1)/4;
                jmlkebkarbohidrat2 = (jmlkebkalori*karbohidrat2)/4;
                kebkarbohidrat.setText(String.format(Locale.US,"%.2f", jmlkebkarbohidrat1)+ " - " +
                        String.format(Locale.US,"%.2f", jmlkebkarbohidrat2)+" gram");

                jmlkebprotein1 = (jmlkebkalori*protein1)/4;
                jmlkebprotein2 = (jmlkebkalori*protein2)/4;
                kebprotein.setText(String.format(Locale.US,"%.2f", jmlkebprotein1)+ " - " +
                        String.format(Locale.US,"%.2f", jmlkebprotein2)+" gram");

                jmlkeblemak1 = (jmlkebkalori*lemak1)/9;
                jmlkeblemak2 = (jmlkebkalori*lemak2)/9;
                keblemak.setText(String.format(Locale.US,"%.2f", jmlkeblemak1)+ " - " +
                        String.format(Locale.US,"%.2f", jmlkeblemak2)+" gram");


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        //pindah bottom navigation
        btn_makanunclick = findViewById(R.id.btn_makanunclick);
        btn_makanunclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(ProfilAct.this,PilihMakananAct.class);
                startActivity(go);
            }
        });

        btn_tentangunclick = findViewById(R.id.btn_tentangunclick);
        btn_tentangunclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(ProfilAct.this,TentangAct.class);
                startActivity(go);
            }
        });

        btn_infounclick = findViewById(R.id.btn_infounclick);
        btn_infounclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(ProfilAct.this,InfoGiziAct.class);
                startActivity(go);
            }
        });


        //pindah lihat profil

        btn_imageView = findViewById(R.id.imageView);
        btn_imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(ProfilAct.this,SeeProfilAct.class);
                startActivity(go);
            }
        });
    }

    public void getUsernameLocal(){
        SharedPreferences sharedPreferences = getSharedPreferences(USERNAME_KEY, MODE_PRIVATE);
        username_key_new =sharedPreferences.getString(username_key, "");

    }

    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            finishAffinity();
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Klik lagi untuk keluar aplikasi", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }
}
