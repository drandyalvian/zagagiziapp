package com.l200150089.creiva.zagagizi;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class AdminEditMakananAct extends AppCompatActivity {

    EditText nama_makanan, jenis_makanan, gram, porsi, kalori, karbohidrat, protein, lemak;
    Button btn_update;
    LinearLayout btn_back;
    DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_edit_makanan);

        nama_makanan = findViewById(R.id.nama_makanan);
        jenis_makanan = findViewById(R.id.jenis_makanan);
        porsi = findViewById(R.id.porsi);
        gram = findViewById(R.id.gram);
        kalori = findViewById(R.id.kalori);
        karbohidrat = findViewById(R.id.karbohidrat);
        protein = findViewById(R.id.protein);
        lemak = findViewById(R.id.lemak);

        btn_back = findViewById(R.id.btn_back);
        btn_update = findViewById(R.id.btn_update);

        reference = FirebaseDatabase.getInstance().getReference();
        final ADataMakanan adataMakanan = (ADataMakanan) getIntent().getSerializableExtra("data");
        if(adataMakanan!=null){
            nama_makanan.setText(adataMakanan.getNama_makanan());
            jenis_makanan.setText(adataMakanan.getJenis_makanan());
            kalori.setText(adataMakanan.getKalori());
            gram.setText(adataMakanan.getGram());
            porsi.setText(adataMakanan.getPorsi());
            karbohidrat.setText(adataMakanan.getKarbohidrat());
            protein.setText(adataMakanan.getProtein());
            lemak.setText(adataMakanan.getLemak());


            btn_update.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    adataMakanan.setNama_makanan(nama_makanan.getText().toString());
                    adataMakanan.setJenis_makanan(jenis_makanan.getText().toString());
                    adataMakanan.setKalori(kalori.getText().toString());
                    adataMakanan.setGram(gram.getText().toString());
                    adataMakanan.setPorsi(porsi.getText().toString());
                    adataMakanan.setKarbohidrat(karbohidrat.getText().toString());
                    adataMakanan.setProtein(protein.getText().toString());
                    adataMakanan.setLemak(lemak.getText().toString());

                    updateInfo(adataMakanan);
                }
            });

        }else {
            btn_update.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    btn_update.setEnabled(false);
                    btn_update.setText("Tunggu...");
                    if (!isEmpty(nama_makanan.getText().toString()) && !isEmpty(jenis_makanan.getText().toString())
                            && !isEmpty(jenis_makanan.getText().toString())&& !isEmpty(kalori.getText().toString())
                            && !isEmpty(gram.getText().toString())&& !isEmpty(porsi.getText().toString())
                            && !isEmpty(karbohidrat.getText().toString())&& !isEmpty(protein.getText().toString())
                            && !isEmpty(lemak.getText().toString())) {

                        //submitInfo(new ADataMakanan(judul_info.getText().toString(), isi_info.getText().toString()));
                    } else {
                        Toast.makeText(getApplicationContext(), "Data tidak boleh kosong", Toast.LENGTH_SHORT).show();
                        InputMethodManager imm = (InputMethodManager)
                                getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(
                                nama_makanan.getWindowToken(), 0);
                    }

                }
            });
        }

        btn_back = findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goseeprofil = new Intent(AdminEditMakananAct.this,AdminDataMakananAct.class);
                startActivity(goseeprofil);
            }
        });

//        //      mengambil data dari intent
//        Bundle bundle = getIntent().getExtras();
//        final String nama_makanan_baru = bundle.getString("nama_makanan");
//
//        reference = FirebaseDatabase.getInstance().getReference()
//                .child("DataMakanan").child(nama_makanan_baru);
//        reference.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                nama_makanan.setText(dataSnapshot.child("nama_makanan").getValue().toString());
//                jenis_makanan.setText(dataSnapshot.child("jenis_makanan").getValue().toString());
//                gram.setText(dataSnapshot.child("gram").getValue().toString());
//                porsi.setText(dataSnapshot.child("porsi").getValue().toString());
//                kalori.setText(dataSnapshot.child("kalori").getValue().toString());
//                karbohidrat.setText(dataSnapshot.child("karbohidrat").getValue().toString());
//                protein.setText(dataSnapshot.child("protein").getValue().toString());
//                lemak.setText(dataSnapshot.child("lemak").getValue().toString());
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });
//
//
//        btn_update.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                //loading
//                btn_update.setEnabled(false);
//                btn_update.setText("Tunggu...");
//
//                //Simpan Database
//                reference.addListenerForSingleValueEvent(new ValueEventListener() {
//                    @Override
//                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                        dataSnapshot.getRef().child("nama_makanan").setValue(nama_makanan.getText().toString());
//                        dataSnapshot.getRef().child("jenis_makanan").setValue(jenis_makanan.getText().toString());
//                        dataSnapshot.getRef().child("gram").setValue(gram.getText().toString());
//                        dataSnapshot.getRef().child("porsi").setValue(porsi.getText().toString());
//                        dataSnapshot.getRef().child("kalori").setValue(kalori.getText().toString());
//                        dataSnapshot.getRef().child("karbohidrat").setValue(karbohidrat.getText().toString());
//                        dataSnapshot.getRef().child("protein").setValue(protein.getText().toString());
//                        dataSnapshot.getRef().child("lemak").setValue(lemak.getText().toString());
//
//                    }
//
//                    @Override
//                    public void onCancelled(@NonNull DatabaseError databaseError) {
//
//                    }
//                });
//                //pindah activity
//                Intent go=new Intent(AdminEditMakananAct.this,AdminDataMakananAct.class);
//                startActivity(go);
//
//
//            }
//        });
    }

    private boolean isEmpty(String s){
        // Cek apakah ada fields yang kosong, sebelum disubmit
        return TextUtils.isEmpty(s);
    }

    private void updateInfo(ADataMakanan adataMakanan) {

        reference.child("DataMakanan") //akses parent index, ibaratnya seperti nama tabel
                .child(adataMakanan.getKey()) //select barang berdasarkan key
                .setValue(adataMakanan) //set value barang yang baru
                .addOnSuccessListener(this, new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {


                        Intent go = new Intent(AdminEditMakananAct.this,AdminDataMakananAct.class);
                        startActivity(go);

                    }
                });
    }

//    private void submitInfo(ADataMakanan adataMakanan) {
//
//        reference.child("DataMakanan").push().setValue(adataMakanan).addOnSuccessListener(this, new OnSuccessListener<Void>() {
//            @Override
//            public void onSuccess(Void aVoid) {
//                nama_makanan.setText("");
//                jenis_makanan.setText("");
//                porsi.setText("");
//                gram.setText("");
//                kalori.setText("");
//                protein.setText("");
//                karbohidrat.setText("");
//                lemak.setText("");
//
//                Toast.makeText(getApplicationContext(),"Sukses", Toast.LENGTH_SHORT).show();
//
//            }
//        });
//    }

    public static Intent getActIntent(Activity activity){
        return new Intent(activity, AdminEditMakananAct.class);
    }
}
