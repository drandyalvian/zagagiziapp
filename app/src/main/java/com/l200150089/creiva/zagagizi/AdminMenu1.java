package com.l200150089.creiva.zagagizi;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class AdminMenu1 extends AppCompatActivity {

    ImageView menu2, picuser, picdm, picinfo;
    LinearLayout lmenu2;
    TextView nama_admin,
            exp,ep,da,so;

    DatabaseReference reference;
    String USERNAME_KEY = "usernamekey";
    String username_key = "";
    String username_key_new ="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_menu1);

        getUsernameLocal();

        picuser = findViewById(R.id.picuser);
        picdm = findViewById(R.id.picdm);
        picinfo = findViewById(R.id.picinfo);
        menu2 = findViewById(R.id.menu2);
        lmenu2 =  findViewById(R.id.lmenu2);
        nama_admin = findViewById(R.id.nama_admin);

        exp = findViewById(R.id.exp);
        da = findViewById(R.id.da);
        ep = findViewById(R.id.ep);
        so = findViewById(R.id.so);

        //mengambil data
        reference= FirebaseDatabase.getInstance().getReference().
                child("Admin").child(username_key_new);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                nama_admin.setText(dataSnapshot.child("nama_lengkap").getValue().toString());

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        exp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(AdminMenu1.this,AdminMenu2.class);
                startActivity(go);
            }
        });

        da.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(AdminMenu1.this,AdminRegiterAct.class);
                startActivity(go);
            }
        });
        ep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(AdminMenu1.this,AdminEditProfileAct.class);
                startActivity(go);
            }
        });

        so.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(AdminMenu1.this,GetStartedAct.class);
                startActivity(go);
            }
        });

        menu2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(AdminMenu1.this,AdminMenu2.class);
                startActivity(go);
            }
        });
        lmenu2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(AdminMenu1.this,AdminMenu2.class);
                startActivity(go);
            }
        });

        picuser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(AdminMenu1.this,AdminDataUserAct.class);
                startActivity(go);
            }
        });
        picinfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(AdminMenu1.this,AdminInfoGiziAct.class);
                startActivity(go);
            }
        });
        picdm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(AdminMenu1.this,AdminDataMakananAct.class);
                startActivity(go);
            }
        });
    }
    public void getUsernameLocal(){
        SharedPreferences sharedPreferences = getSharedPreferences(USERNAME_KEY, MODE_PRIVATE);
        username_key_new =sharedPreferences.getString(username_key, "");

    }
}
