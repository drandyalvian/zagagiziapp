package com.l200150089.creiva.zagagizi;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

public class ADataUserAdapter extends RecyclerView.Adapter<ADataUserAdapter.MyViewHolder> {


    FirebaseDataListener listener;

    Context context;
    ArrayList<ADataUser> adataUser;
    public ADataUserAdapter(Context c, ArrayList<ADataUser> p, Context b ){
        context = c;
        adataUser = p;
        listener = (AdminDataUserAct) b;

    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_admin_datauser, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, final int i) {

        final String gnamamalengkap = adataUser.get(i).getNama_lengkap();
        final String gusername = adataUser.get(i).getUsername();

        myViewHolder.nama_lengkap.setText(gnamamalengkap);
        myViewHolder.username.setText(gusername);



        myViewHolder.delbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onDeleteData(adataUser.get(i), i);
            }
        });

    }

    @Override
    public int getItemCount() {
        return adataUser.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        TextView nama_lengkap, username;
        Button delbutton;


        public MyViewHolder(@NonNull View itemView){
            super(itemView);

            nama_lengkap = itemView.findViewById(R.id.nama_lengkap);
            username = itemView.findViewById(R.id.username);

            delbutton = itemView.findViewById(R.id.delbutton);
        }
    }
    public interface FirebaseDataListener{
        void onDeleteData(ADataUser adataUser, int i);
    }

}
