package com.l200150089.creiva.zagagizi;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class AdminDataUserAct extends AppCompatActivity implements ADataUserAdapter.FirebaseDataListener {


    DatabaseReference reference, reference2;
    private RecyclerView rvView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<ADataUser> list2;

    EditText txtsearch;
    Button btn_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_data_user);

        EditText editor = new EditText(this);
        editor.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS);

        txtsearch = findViewById(R.id.txtsearch);
        rvView = (RecyclerView) findViewById(R.id.datauser_place);
        rvView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        rvView.setLayoutManager(layoutManager);

        reference2 = FirebaseDatabase.getInstance()
                .getReference().child("Users");

        reference = FirebaseDatabase.getInstance().getReference();
        reference.child("Users").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                list2 = new ArrayList<>();
                for (DataSnapshot dss1 : dataSnapshot.getChildren()){
                    ADataUser aduser = dss1.getValue(ADataUser.class);
                    aduser.setKey(dss1.getKey());

                    list2.add(aduser);
                }
                adapter = new ADataUserAdapter(AdminDataUserAct.this, list2, AdminDataUserAct.this);
                rvView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        txtsearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable a) {
                if(!a.toString().isEmpty()){
                    search(a.toString());
                }else {
                    search("");
                }


            }
        });

        btn_back = findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(AdminDataUserAct.this,AdminMenu2.class);
                startActivity(go);
            }
        });


    }

    public static Intent getActIntent(Activity activity){
        return new Intent(activity, AdminDataUserAct.class);
    }


    @Override
    public void onDeleteData(ADataUser adataUser, final int i) {

        if(reference!=null){
            reference.child("Users").child(adataUser.getKey()).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Toast.makeText(AdminDataUserAct.this,"success delete", Toast.LENGTH_LONG).show();
                }
            });

        }

    }

    private void search(String a) {

        Query query = reference2.orderByChild("nama_lengkap")
                .startAt(a)
                .endAt(a+"\uf8ff");
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChildren()){
                    list2.clear();
                    for (DataSnapshot dss: dataSnapshot.getChildren()){
                        final ADataUser adataUser = dss.getValue(ADataUser.class);
                        list2.add(adataUser);
                    }

                    ADataUserAdapter adapter = new ADataUserAdapter(AdminDataUserAct.this,list2, AdminDataUserAct.this);
                    rvView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
