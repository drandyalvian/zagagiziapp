package com.l200150089.creiva.zagagizi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class AdminDetailInfoAct extends AppCompatActivity {

    DatabaseReference reference;

    TextView  xjudul_info, xisi_info;
    Button btn_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_detail_info);

        xjudul_info = findViewById(R.id.xjudul_info);
        xisi_info = findViewById(R.id.xisi_info);

        btn_back = findViewById(R.id.btn_back);

        AInfoGizi ainfoGizi = (AInfoGizi) getIntent().getSerializableExtra("data");
        if(ainfoGizi!=null){
            xjudul_info.setText(ainfoGizi.getJudul_info());
            xisi_info.setText(ainfoGizi.getIsi_info());
        }

////        mengambil data dari intent
//        Bundle bundle = getIntent().getExtras();
//        final String judul_info_baru = bundle.getString("judul_info");
//
////        Mengambil data dari firebase
//        reference = FirebaseDatabase.getInstance().getReference().child("InfoGizis").child(judul_info_baru);
//        reference.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                xjudul_info.setText(dataSnapshot.child("judul_info").getValue().toString());
//                xisi_info.setText(dataSnapshot.child("isi_info").getValue().toString());
//
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });

    }
    public static Intent getActIntent(Activity activity){
        return new Intent(activity, AdminDetailInfoAct.class);
    }
}
