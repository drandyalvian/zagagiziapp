package com.l200150089.creiva.zagagizi;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class AdminDataMakananAct extends AppCompatActivity implements ADataMakananAdapter.FirebaseDataListener {


    DatabaseReference reference, reference2;
    private RecyclerView rvView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<ADataMakanan> list2;


    EditText txtsearch;
    Button addmakanan;
    Button btn_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_data_makanan);

        EditText editor = new EditText(this);
        editor.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS);

        txtsearch = findViewById(R.id.txtsearch);
        addmakanan = findViewById(R.id.addmakanan);

        rvView = (RecyclerView) findViewById(R.id.datamakanan_place);
        rvView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        rvView.setLayoutManager(layoutManager);

        reference2 = FirebaseDatabase.getInstance()
                .getReference().child("DataMakanan");

        reference = FirebaseDatabase.getInstance().getReference();
        reference.child("DataMakanan").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                list2 = new ArrayList<>();
                for (DataSnapshot dataSnapshot1: dataSnapshot.getChildren()){

                    ADataMakanan admakanan = dataSnapshot1.getValue(ADataMakanan.class);
                    admakanan.setKey(dataSnapshot1.getKey());

                    list2.add(admakanan);

                }
                adapter = new ADataMakananAdapter( AdminDataMakananAct.this, list2, AdminDataMakananAct.this);
                rvView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        txtsearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().isEmpty()){
                    search(s.toString());
                }else {
                    search("");
                }


            }
        });

        //pindah activity
        addmakanan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go=new Intent(AdminDataMakananAct.this,AdminAddMakananAct.class);
                startActivity(go);
            }
        });

        btn_back = findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goseeprofil = new Intent(AdminDataMakananAct.this,AdminMenu2.class);
                startActivity(goseeprofil);
            }
        });

    }

    public static Intent getActIntent(Activity activity){
        return new Intent(activity, AdminDataMakananAct.class);
    }

    public void onDeleteData(ADataMakanan adataMakanan, final int i){
        if(reference!=null){
            reference.child("DataMakanan").child(adataMakanan.getKey()).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Toast.makeText(AdminDataMakananAct.this,"success delete", Toast.LENGTH_LONG).show();
                }
            });

        }
    }
    private void search(String s) {

        Query query = reference2.orderByChild("nama_makanan")
                .startAt(s)
                .endAt(s+"\uf8ff");
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChildren()){
                    list2.clear();
                    for (DataSnapshot dss: dataSnapshot.getChildren()){
                        final ADataMakanan adataMakanan = dss.getValue(ADataMakanan.class);
                        list2.add(adataMakanan);
                    }

                    ADataMakananAdapter adapter = new ADataMakananAdapter(AdminDataMakananAct.this,list2, AdminDataMakananAct.this);
                    rvView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
