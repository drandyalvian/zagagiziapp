package com.l200150089.creiva.zagagizi;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class InfoGiziAdapter extends RecyclerView.Adapter<InfoGiziAdapter.MyViewHolder> {

    Context context;
    ArrayList<InfoGizi> infoGizi;
    public InfoGiziAdapter(Context c, ArrayList<InfoGizi> p){
        context = c;
        infoGizi = p;
    }


    @NonNull
    @Override
    public InfoGiziAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new MyViewHolder(LayoutInflater.
        from(context).inflate(R.layout.item_infogizi,
                viewGroup,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        myViewHolder.xjudul_infogizi.setText(infoGizi.get(i).getJudul_info());

        final String getJudulInfo = infoGizi.get(i).getJudul_info();
        final String getkey = infoGizi.get(i).getKey();

        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goToDetailInfo = new Intent(context, DetailInfoAct.class);
                goToDetailInfo.putExtra("key",getkey);
                context.startActivity(goToDetailInfo);
            }
        });

    }

    @Override
    public int getItemCount() {
        return infoGizi.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        TextView xjudul_infogizi;

        public MyViewHolder(@NonNull View itemView){
            super(itemView);

            xjudul_infogizi = itemView.findViewById(R.id.xjudul_infogizi);
        }
    }
}
