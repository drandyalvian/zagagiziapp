package com.l200150089.creiva.zagagizi;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class PilihMakananAct extends AppCompatActivity implements PilihMakananAdapter.FirebaseDataListener {

    Button btn_profilunclick;
    Button btn_tentangunclick;
    Button btn_infounclick;

    Button btn_hitung;
    ImageView btn_tambah;

    DatabaseReference reference;
    private RecyclerView rvView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<PilihMakanan> pilihMakanan;


    String USERNAME_KEY = "usernamekey";
    String username_key ="";
    String username_key_new ="";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_pilih_makanan);

        rvView = (RecyclerView) findViewById(R.id.pilihmakanan_place);
        rvView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        rvView.setLayoutManager(layoutManager);

        getUsernameLocal();


        reference = FirebaseDatabase.getInstance().getReference();
        reference.child("Makananku").child(username_key_new).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                pilihMakanan = new ArrayList<>();
                for (DataSnapshot dataSnapshot1: dataSnapshot.getChildren()){

                    PilihMakanan pmakanan = dataSnapshot1.getValue(PilihMakanan.class);
                    pmakanan.setKey(dataSnapshot1.getKey());

                    pilihMakanan.add(pmakanan);

                }
                adapter = new PilihMakananAdapter(pilihMakanan, PilihMakananAct.this);
                rvView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });




//pindah activity Bottom navigation
        btn_profilunclick = findViewById(R.id.btn_profilunclick);
        btn_profilunclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(PilihMakananAct.this,ProfilAct.class);
                startActivity(go);
            }
        });

        btn_tentangunclick = findViewById(R.id.btn_tentangunclick);
        btn_tentangunclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(PilihMakananAct.this,TentangAct.class);
                startActivity(go);
            }
        });

        btn_infounclick = findViewById(R.id.btn_infounclick);
        btn_infounclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(PilihMakananAct.this,InfoGiziAct.class);
                startActivity(go);
            }
        });

//Pindah ke hitung

        btn_hitung = findViewById(R.id.btn_hitung);
        btn_hitung.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gohitung = new Intent(PilihMakananAct.this, HitungAct.class);
                startActivity(gohitung);
            }
        });

//Pindah ke data makanan
        btn_tambah = findViewById(R.id.btn_tambah);
        btn_tambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent godatamakan = new Intent(PilihMakananAct.this, DataMakananAct.class);
                startActivity(godatamakan);
            }
        });



    }


    public static Intent getActIntent(Activity activity){
        return new Intent(activity, PilihMakananAct.class);
    }

    public void onDeleteData(PilihMakanan pilihMakanan, final int i){
        if(reference!=null){
            reference.child("Makananku").child(username_key_new).child(pilihMakanan.getKey()).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Toast.makeText(PilihMakananAct.this,"success delete", Toast.LENGTH_LONG).show();
                }
            });

        }
    }
    public void getUsernameLocal(){
        SharedPreferences sharedPreferences = getSharedPreferences(USERNAME_KEY, MODE_PRIVATE);
        username_key_new =sharedPreferences.getString(username_key, "");

    }


}
