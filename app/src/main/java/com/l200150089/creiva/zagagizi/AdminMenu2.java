package com.l200150089.creiva.zagagizi;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class AdminMenu2 extends AppCompatActivity {

    ImageView menu1, picuser, picdm, picinfo;
    TextView nama_admin,textuser,textdm,textinfo;


    DatabaseReference reference;
    String USERNAME_KEY = "usernamekey";
    String username_key = "";
    String username_key_new ="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_menu2);

        getUsernameLocal();

        picuser = findViewById(R.id.picuser);
        picdm = findViewById(R.id.picdm);
        picinfo = findViewById(R.id.picinfo);
        menu1 = findViewById(R.id.menu1);

        textuser = findViewById(R.id.textuser);
        textdm = findViewById(R.id.textdm);
        textinfo = findViewById(R.id.textinfo);
        nama_admin = findViewById(R.id.nama_admin);

        //mengambil data
        reference= FirebaseDatabase.getInstance().getReference().
                child("Admin").child(username_key_new);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                nama_admin.setText(dataSnapshot.child("nama_lengkap").getValue().toString());

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        menu1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent adatauser = new Intent(AdminMenu2.this,AdminMenu1.class);
                startActivity(adatauser);
            }
        });

        picuser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent adatauser = new Intent(AdminMenu2.this,AdminDataUserAct.class);
                startActivity(adatauser);
            }
        });
        textuser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent adatauser = new Intent(AdminMenu2.this,AdminDataUserAct.class);
                startActivity(adatauser);
            }
        });

        picdm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(AdminMenu2.this,AdminDataMakananAct.class);
                startActivity(go);
            }
        });
        textdm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(AdminMenu2.this,AdminDataMakananAct.class);
                startActivity(go);
            }
        });

        picinfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(AdminMenu2.this,AdminInfoGiziAct.class);
                startActivity(go);
            }
        });
        textinfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(AdminMenu2.this,AdminInfoGiziAct.class);
                startActivity(go);
            }
        });

    }
    public void getUsernameLocal(){
        SharedPreferences sharedPreferences = getSharedPreferences(USERNAME_KEY, MODE_PRIVATE);
        username_key_new =sharedPreferences.getString(username_key, "");

    }
}
