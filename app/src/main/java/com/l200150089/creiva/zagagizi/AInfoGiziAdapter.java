package com.l200150089.creiva.zagagizi;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class AInfoGiziAdapter extends RecyclerView.Adapter<AInfoGiziAdapter.MyViewHolder> {

    FirebaseDataListener listener;

    Context context;
    ArrayList<AInfoGizi> ainfoGizi;
    public AInfoGiziAdapter(Context c, ArrayList<AInfoGizi> p, Context b ){
        context = c;
        ainfoGizi = p;

        listener = (AdminInfoGiziAct) b;

    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_admin_infogizi, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, final int i) {



        final String gjudul = ainfoGizi.get(i).getJudul_info();

        System.out.println("DATA one by one "+i+ainfoGizi.size());

        myViewHolder.judul_info.setText(gjudul);

        //lihat data
        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(AdminDetailInfoAct.getActIntent((Activity) context)
                        .putExtra("data", ainfoGizi.get(i)));
            }
        });

        myViewHolder.delbutton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        final Dialog dialog = new Dialog(context);
                        dialog.setContentView(R.layout.dialog_view);
                        dialog.setTitle("Pilih Aksi");
                        dialog.show();

                        Button delbtn = (Button) dialog.findViewById(R.id.dialogdelete);
                        Button editbtn = (Button) dialog.findViewById(R.id.dialogedit);

                        delbtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                listener.onDeleteData(ainfoGizi.get(i), i);
                            }
                        });

                        editbtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                context.startActivity(AdminEditInfoAct.getActIntent((Activity) context)
                                        .putExtra("data", ainfoGizi.get(i)));

                            }
                        });

                    }
                }
        );

    }

    @Override
    public int getItemCount() {
        return ainfoGizi.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        TextView judul_info;
        ImageView delbutton;


        public MyViewHolder(@NonNull View itemView){
            super(itemView);

            judul_info = itemView.findViewById(R.id.judul_info);

            delbutton = itemView.findViewById(R.id.delbutton);
        }
    }
    public interface FirebaseDataListener{
        void onDeleteData(AInfoGizi ainFoGizi, int i);
    }
}
