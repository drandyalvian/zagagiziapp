package com.l200150089.creiva.zagagizi;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class DetailInfoAct extends AppCompatActivity {

    DatabaseReference reference;

    Button btn_profilunclick;
    Button btn_infounclick;
    Button btn_makanunclick;
    Button btn_tentangunclick;

    TextView  xjudul_info, xisi_info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_info);

        xjudul_info = findViewById(R.id.xjudul_info);
        xisi_info = findViewById(R.id.xisi_info);

//        mengambil data dari intent
        Bundle bundle = getIntent().getExtras();
        final String judul_info_baru = bundle.getString("key");

//        Mengambil data dari firebase
        reference = FirebaseDatabase.getInstance().getReference().child("InfoGizis").child(judul_info_baru);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                xjudul_info.setText(dataSnapshot.child("judul_info").getValue().toString());
                xisi_info.setText(dataSnapshot.child("isi_info").getValue().toString());

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

// Pindah acivity Bottom Navigation
        btn_profilunclick = findViewById(R.id.btn_profilunclick);
        btn_profilunclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(DetailInfoAct.this,ProfilAct.class);
                startActivity(go);
            }
        });

        btn_makanunclick = findViewById(R.id.btn_makanunclick );
        btn_makanunclick .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(DetailInfoAct.this,PilihMakananAct.class);
                startActivity(go);
            }
        });

        btn_infounclick = findViewById(R.id.btn_infounclick);
        btn_infounclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(DetailInfoAct.this,InfoGiziAct.class);
                startActivity(go);
            }
        });

        btn_tentangunclick = findViewById(R.id.btn_tentangunclick);
        btn_tentangunclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(DetailInfoAct.this,TentangAct.class);
                startActivity(go);
            }
        });


    }
}
