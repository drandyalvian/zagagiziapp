package com.l200150089.creiva.zagagizi;

import java.io.Serializable;

public class PilihMakanan implements Serializable {
    String nama_makanan, jenis_makanan, porsi, gram, kalori, karbohidrat, protein, lemak, key;
    public PilihMakanan() {
    }

    public PilihMakanan(String key, String nama_makanan, String jenis_makanan, String porsi, String gram, String kalori, String karbohidrat, String protein, String lemak) {
        this.nama_makanan = nama_makanan;
        this.jenis_makanan = jenis_makanan;
        this.porsi = porsi;
        this.gram = gram;
        this.kalori = kalori;
        this.karbohidrat = karbohidrat;
        this.protein = protein;
        this.lemak = lemak;
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getNama_makanan() {
        return nama_makanan;
    }

    public void setNama_makanan(String nama_makanan) {
        this.nama_makanan = nama_makanan;
    }

    public String getJenis_makanan() {
        return jenis_makanan;
    }

    public void setJenis_makanan(String jenis_makanan) {
        this.jenis_makanan = jenis_makanan;
    }

    public String getPorsi() {
        return porsi;
    }

    public void setPorsi(String porsi) {
        this.porsi = porsi;
    }

    public String getGram() {
        return gram;
    }

    public void setGram(String gram) {
        this.gram = gram;
    }

    public String getKalori() {
        return kalori;
    }

    public void setKalori(String kalori) {
        this.kalori = kalori;
    }

    public String getKarbohidrat() {
        return karbohidrat;
    }

    public void setKarbohidrat(String karbohidrat) {
        this.karbohidrat = karbohidrat;
    }

    public String getProtein() {
        return protein;
    }

    public void setProtein(String protein) {
        this.protein = protein;
    }

    public String getLemak() {
        return lemak;
    }

    public void setLemak(String lemak) {
        this.lemak = lemak;
    }
}
