package com.l200150089.creiva.zagagizi;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class SuccesRegisterAct extends AppCompatActivity {

    Button btn_explore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_succes_register);

        btn_explore =findViewById(R.id.btn_explore);

        btn_explore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // setting timer untuk 1 detik
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // merubah activity ke activity lain
                        Intent gotoprofil = new Intent(SuccesRegisterAct.this,ProfilAct.class);
                        startActivity(gotoprofil);
                        finish();
                    }
                }, 1000); // 1000 ms = 1s



            }
        });
    }
}
