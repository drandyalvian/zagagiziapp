package com.l200150089.creiva.zagagizi;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class AdminEditInfoAct extends AppCompatActivity {

    EditText judul_info, isi_info;
    Button btn_update;
    LinearLayout btn_back;
    DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_edit_info);

        judul_info = findViewById(R.id.judul_info);
        isi_info = findViewById(R.id.isi_info);

        btn_back = findViewById(R.id.btn_back);
        btn_update = findViewById(R.id.btn_update);

        reference = FirebaseDatabase.getInstance().getReference();
        final AInfoGizi ainfoGizi = (AInfoGizi) getIntent().getSerializableExtra("data");
        if(ainfoGizi!=null){
            judul_info.setText(ainfoGizi.getJudul_info());
            isi_info.setText(ainfoGizi.getIsi_info());

            btn_update.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ainfoGizi.setJudul_info(judul_info.getText().toString());
                    ainfoGizi.setIsi_info(isi_info.getText().toString());

                    updateInfo(ainfoGizi);
                }
            });

        }else {
            btn_update.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    btn_update.setEnabled(false);
                    btn_update.setText("Tunggu...");
                    if(!isEmpty(judul_info.getText().toString()) && !isEmpty(isi_info.getText().toString())) {

                        submitInfo(new AInfoGizi(judul_info.getText().toString(), isi_info.getText().toString()));
                    }
                    else{
                        Toast.makeText(getApplicationContext(),"Data tidak boleh kosong", Toast.LENGTH_SHORT).show();
                        InputMethodManager imm = (InputMethodManager)
                                getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(
                                judul_info.getWindowToken(), 0);
                    }

                }
            });
        }

        btn_back = findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goseeprofil = new Intent(AdminEditInfoAct.this,AdminInfoGiziAct.class);
                startActivity(goseeprofil);
            }
        });

//        //      mengambil data dari intent
//        Bundle bundle = getIntent().getExtras();
//        final String judul_info_baru = bundle.getString("judul_info");
//
//
//        reference = FirebaseDatabase.getInstance().getReference()
//                .child("InfoGizis").child(judul_info_baru);
//        reference.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                judul_info.setText(dataSnapshot.child("judul_info").getValue().toString());
//                isi_info.setText(dataSnapshot.child("isi_info").getValue().toString());
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });

//        btn_update.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                //loading
//                btn_update.setEnabled(false);
//                btn_update.setText("Tunggu...");
//
//
//                reference.addListenerForSingleValueEvent(new ValueEventListener() {
//                    @Override
//                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                        dataSnapshot.getRef().child("judul_info").setValue(judul_info.getText().toString());
//                        dataSnapshot.getRef().child("isi_info").setValue(isi_info.getText().toString());
//
//
//
//                    }
//
//                    @Override
//                    public void onCancelled(@NonNull DatabaseError databaseError) {
//
//                    }
//                });
//
//                //pindah activity
//                Intent go=new Intent(AdminEditInfoAct.this,AdminInfoGiziAct.class);
//                startActivity(go);
//            }
//        });
    }

    private boolean isEmpty(String s){
        // Cek apakah ada fields yang kosong, sebelum disubmit
        return TextUtils.isEmpty(s);
    }

    private void updateInfo(AInfoGizi ainfoGizi) {

        reference.child("InfoGizis") //akses parent index, ibaratnya seperti nama tabel
                .child(ainfoGizi.getKey()) //select barang berdasarkan key
                .setValue(ainfoGizi) //set value barang yang baru
                .addOnSuccessListener(this, new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {


                        Intent go = new Intent(AdminEditInfoAct.this,AdminInfoGiziAct.class);
                        startActivity(go);

                    }
                });
    }

    private void submitInfo(AInfoGizi ainfoGizi) {

        reference.child("InfoGizis").push().setValue(ainfoGizi).addOnSuccessListener(this, new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                judul_info.setText("");
                isi_info.setText("");

                Toast.makeText(getApplicationContext(),"Sukses", Toast.LENGTH_SHORT).show();

            }
        });
    }

    public static Intent getActIntent(Activity activity){
        return new Intent(activity, AdminEditInfoAct.class);
    }
}
