package com.l200150089.creiva.zagagizi;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class SeeProfilAct extends AppCompatActivity {

    //firebase
    TextView nama_lengkap, jenis_kelamin, aktivitas, tinggi_badan, berat_badan, usia;
    DatabaseReference reference;
    String USERNAME_KEY = "usernamekey";
    String username_key = "";
    String username_key_new ="";

    //act
    Button btn_profilunclick;
    Button btn_infounclick;
    Button btn_makanunclick;
    Button btn_tentangunclick;

    Button btn_editprofil;
    Button btn_keluar;

    ScrollView parentScrollView;
    ScrollView childScrollView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_see_profil);

        getUsernameLocal();
        nama_lengkap = findViewById(R.id.nama_lengkap);
        jenis_kelamin = findViewById(R.id.jenis_kelamin);
        aktivitas = findViewById(R.id.aktivitas);
        tinggi_badan = findViewById(R.id.tinggi_badan);
        berat_badan = findViewById(R.id.berat_badan);
        usia = findViewById(R.id.usia);
        reference= FirebaseDatabase.getInstance().getReference().
                child("Users").child(username_key_new);
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                nama_lengkap.setText(dataSnapshot.child("nama_lengkap").getValue().toString());
                jenis_kelamin.setText(dataSnapshot.child("jenis_kelamin").getValue().toString());
                aktivitas.setText(dataSnapshot.child("aktivitas").getValue().toString());
                tinggi_badan.setText(dataSnapshot.child("tinggi_badan").getValue().toString());
                berat_badan.setText(dataSnapshot.child("berat_badan").getValue().toString());
                usia.setText(dataSnapshot.child("usia").getValue().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


//pindah bottom navigation
        btn_profilunclick = findViewById(R.id.btn_profilunclick);
        btn_profilunclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(SeeProfilAct.this,ProfilAct.class);
                startActivity(go);
            }
        });

        btn_makanunclick = findViewById(R.id.btn_makanunclick );
        btn_makanunclick .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(SeeProfilAct.this,PilihMakananAct.class);
                startActivity(go);
            }
        });

        btn_infounclick = findViewById(R.id.btn_infounclick);
        btn_infounclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(SeeProfilAct.this,InfoGiziAct.class);
                startActivity(go);
            }
        });

        btn_tentangunclick = findViewById(R.id.btn_tentangunclick);
        btn_tentangunclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(SeeProfilAct.this,TentangAct.class);
                startActivity(go);
            }
        });

//pindah ke edit profil
        btn_editprofil = findViewById(R.id.btn_editprofil);
        btn_editprofil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(SeeProfilAct.this,EditProfilAct.class);
                startActivity(go);
            }
        });

// ke get started
        btn_keluar = findViewById(R.id.btn_keluar);
        btn_keluar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(SeeProfilAct.this,GetStartedAct.class);
                startActivity(go);
            }
        });

// dua scrollview konek
        parentScrollView = (ScrollView) findViewById(R.id.scrollsee1);
        childScrollView = (ScrollView) findViewById(R.id.scrollsee2);

        parentScrollView.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View p_v, MotionEvent p_event) {
                childScrollView.getParent().requestDisallowInterceptTouchEvent(
                        false);
                // We will have to follow above for all scrollable contents
                return false;
            }
        });
        childScrollView.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View p_v, MotionEvent p_event) {
                // this will disallow the touch request for parent scroll on
                // touch of child view
                p_v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
    }

    public void getUsernameLocal(){
        SharedPreferences sharedPreferences = getSharedPreferences(USERNAME_KEY, MODE_PRIVATE);
        username_key_new =sharedPreferences.getString(username_key, "");

    }
}
