package com.l200150089.creiva.zagagizi;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class DataMakananAdapter extends RecyclerView.Adapter<DataMakananAdapter.MyViewHolder> {

    Context context;
    ArrayList<DataMakanan> dataMakanan;
    public DataMakananAdapter(Context c, ArrayList<DataMakanan> p){
        context = c;
        dataMakanan = p;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public DataMakananAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new MyViewHolder(LayoutInflater.
                from(context).inflate(R.layout.item_datamakanan,
                viewGroup,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {

        myViewHolder.xnama_makanan.setText(dataMakanan.get(i).getNama_makanan());
        myViewHolder.xjenis_makanan.setText(dataMakanan.get(i).getJenis_makanan());

        final String getNamaMakanan = dataMakanan.get(i).getNama_makanan();
        final String getkey = dataMakanan.get(i).getKey();

        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goToDetailMakanan = new Intent(context, DetailMakananAct.class);
                goToDetailMakanan.putExtra("key",getkey);
                context.startActivity(goToDetailMakanan);
            }
        });

    }

    @Override
    public int getItemCount() {
        return dataMakanan.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        TextView xnama_makanan,  xjenis_makanan;


        public MyViewHolder(@NonNull View itemView){
            super(itemView);

            xnama_makanan = itemView.findViewById(R.id.xnama_makanan);
            xjenis_makanan = itemView.findViewById(R.id.xjenis_makanan);
        }
    }
}