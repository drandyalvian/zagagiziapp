package com.l200150089.creiva.zagagizi;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.lang.ref.Reference;

public class RegisterOneAct extends AppCompatActivity {
    //act
    Button btn_continue;
    LinearLayout btn_back;

    //firebase
    EditText username, password;
    DatabaseReference reference,reference1;

    String USERNAME_KEY = "usernamekey";
    String username_key ="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_one);

        username=findViewById(R.id.username);
        password=findViewById(R.id.password);

        username.addTextChangedListener(loginTextWatcher);
        password.addTextChangedListener(loginTextWatcher);


        btn_continue=findViewById(R.id.btn_continue);



        btn_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //loading
                btn_continue.setEnabled(false);
                btn_continue.setText("Tunggu...");

                final String text = username.getText().toString().trim();
                Query reference1 = FirebaseDatabase.getInstance().getReference().child("Users").orderByChild("username").equalTo(text);
                reference1.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            Toast toast = Toast.makeText(RegisterOneAct.this,
                                    "Username Sudah Ada", Toast.LENGTH_LONG);
                            toast.show();
                            btn_continue.setEnabled(true);
                            btn_continue.setText("Lanjut");
                        }else{
                            //menyimpan data local storage
                            SharedPreferences sharedPreferences = getSharedPreferences(USERNAME_KEY, MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString(username_key, username.getText().toString());
                            editor.apply();

                            //Simpan Database
                            reference = FirebaseDatabase.getInstance().getReference()
                                    .child("Users").child(username.getText().toString());
                            reference.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    dataSnapshot.getRef().child("username").setValue(username.getText().toString());
                                    dataSnapshot.getRef().child("password").setValue(password.getText().toString());
                                    dataSnapshot.getRef().child("key").setValue(username.getText().toString());
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });

                            //loading
                            btn_continue.setEnabled(false);
                            btn_continue.setText("Tunggu...");

                            ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                            android.net.NetworkInfo wifi = cm
                                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
                            android.net.NetworkInfo datac = cm
                                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
                            if((wifi != null & datac != null) && (wifi.isConnected()| datac.isConnected())){
                                //pindah activity
                                Intent gotoregistwo=new Intent(RegisterOneAct.this,RegisterTwoAct.class);
                                startActivity(gotoregistwo);
                            }else {
                                Toast toast = Toast.makeText(RegisterOneAct.this,
                                        "Tidak ada koneksi, pastikan koneksi anda terhubung dan tidak buruk", Toast.LENGTH_LONG);
                                toast.show();
                                btn_continue.setEnabled(true);
                                btn_continue.setText("Lanjut");
                            }

                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


            }
        });

        btn_back = findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goseeprofil = new Intent(RegisterOneAct.this,GetStartedAct.class);
                startActivity(goseeprofil);
            }
        });
    }

    private TextWatcher loginTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String usernameInput = username.getText().toString().trim();
            String passwordInput = password.getText().toString().trim();

            btn_continue.setEnabled(!usernameInput.isEmpty() && !passwordInput.isEmpty());





        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };


}
