package com.l200150089.creiva.zagagizi;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Locale;

public class HitungAct extends AppCompatActivity {

    Button btn_profilunclick;
    Button btn_infounclick;
    Button btn_makanunclick;
    Button btn_tentangunclick;

    DatabaseReference reference, reference2;
    String USERNAME_KEY = "usernamekey";
    String username_key = "";
    String username_key_new ="";

    TextView textkalori, textkarbohidrat, textprotein, textlemak;
    TextView refresh;

    //IMT
    double imt = 0;
    double dbberatbadan = 0;
    double dbtinggibadan = 0;
    //AMB
    Integer dbusia =0;
    String dbgender = "";
    double amb =0;
    //Aktivitas
    String dbaktivitas = "";
    double xaktivitas = 0;

    //kebutuhan nutrisi
    double jmlkebkalori,jmlkebkarbohidrat1,jmlkebprotein1, jmlkeblemak1,
            jmlkebkarbohidrat2,jmlkebprotein2, jmlkeblemak2 = 0;
    double karbohidrat1 = 0.60;
    double karbohidrat2 = 0.75;
    double protein1 = 0.10;
    double protein2 = 0.15;
    double lemak1 = 0.10;
    double lemak2 = 0.25;

    //hitung makanan
    double jmlkalorimakanan,jmlkarbohidratmakanan ,jmlproteinmakanan, jmllemakmakanan = 0;

    double minbataskalori, maxbataskalori, minbataskarbo, maxbataskarbo,
            minbatasprotein, maxbatasprotein, minbataslemak, maxbataslemak = 0;

    //keputusan
    Button hasilkalorimakanan, hasilkarbohidratmakanan, hasilproteinmakanan, hasillemakmakanan;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hitung);

        getUsernameLocal();

        refresh = findViewById(R.id.refresh);

        textkalori = findViewById(R.id.textkalori);
        textkarbohidrat = findViewById(R.id.textkarbohidrat);
        textprotein = findViewById(R.id.textprotein);
        textlemak = findViewById(R.id.textlemak);

        hasilkalorimakanan = findViewById(R.id.hasilkalorimakanan);
        hasilkarbohidratmakanan = findViewById(R.id.hasilkarbohidratmakanan);
        hasilproteinmakanan = findViewById(R.id.hasilproteinmakanan);
        hasillemakmakanan = findViewById(R.id.hasillemakmakanan);

        Toast toast = Toast.makeText(HitungAct.this,
                "Klik Refresh, Untuk Menampilkan Keputusan Hasil Hitung", Toast.LENGTH_LONG);
        toast.show();


        reference2 = FirebaseDatabase.getInstance().getReference().
                child("Users").child(username_key_new);
        reference2.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //IMT
                dbberatbadan = Integer.valueOf(dataSnapshot.child("berat_badan").getValue().toString());
                dbtinggibadan = Integer.valueOf(dataSnapshot.child("tinggi_badan").getValue().toString());
                imt = dbberatbadan / ((dbtinggibadan/100) *(dbtinggibadan/100));

                //AMB
                dbusia = Integer.valueOf(dataSnapshot.child("usia").getValue().toString());
                dbgender = dataSnapshot.child("jenis_kelamin").getValue().toString();
                if (dbgender.equals("Laki - laki")){
                    amb = 66 + (13.7 * dbberatbadan)+(5*dbtinggibadan)-(6.8*dbusia);
                }
                if (dbgender.equals("Perempuan")){
                    amb = 65.5 + (9.6 * dbberatbadan)+(1.8*dbtinggibadan)-(4.7*dbusia);
                }

                //AKTIVITAS
                dbaktivitas = dataSnapshot.child("aktivitas").getValue().toString();

                if (dbgender.equals("Laki - laki") ){
                    if (dbaktivitas.equals("Sangat Ringan")){
                        xaktivitas = 1.30;
                    }else if (dbaktivitas.equals("Ringan")){
                        xaktivitas = 1.65;
                    }else if (dbaktivitas.equals("Sedang")){
                        xaktivitas = 1.76;
                    }else if (dbaktivitas.equals("Berat")){
                        xaktivitas = 2.10;
                    }else {
                        Toast.makeText(getApplicationContext(),"Null!", Toast.LENGTH_SHORT).show();
                    }
                }
                if (dbgender.equals("Perempuan")){
                    if (dbaktivitas.equals("Sangat Ringan") ){
                        xaktivitas = 1.30;
                    }else if (dbaktivitas.equals("Ringan") ){
                        xaktivitas = 1.55;
                    }else if (dbaktivitas.equals("Sedang")){
                        xaktivitas = 1.70;
                    }else if (dbaktivitas.equals("Berat")){
                        xaktivitas = 2.00;
                    }else {
                        Toast.makeText(getApplicationContext(),"Null!", Toast.LENGTH_SHORT).show();
                    }
                }

                //kebkalori
                if (imt < 18.5 ){
                    jmlkebkalori = (xaktivitas * amb)+500;
                }
                if (imt > 18.5 && imt <= 25.0 ){
                    jmlkebkalori = xaktivitas * amb;
                }

                if (imt > 25.0 ){
                    jmlkebkalori = (xaktivitas * amb)- 500;
                }

                //karbohidrat
                jmlkebkarbohidrat1 = (jmlkebkalori*karbohidrat1)/4;
                jmlkebkarbohidrat2 = (jmlkebkalori*karbohidrat2)/4;

                //kebprotein
                jmlkebprotein1 = (jmlkebkalori*protein1)/4;
                jmlkebprotein2 = (jmlkebkalori*protein2)/4;

                //keblemak
                jmlkeblemak1 = (jmlkebkalori*lemak1)/9;
                jmlkeblemak2 = (jmlkebkalori*lemak2)/9;


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        reference= FirebaseDatabase.getInstance().getReference().
                child("Makananku").child(username_key_new);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot ds : dataSnapshot.getChildren()){
                    double dbjumlahkalori = Double.parseDouble(String.valueOf(ds.child("kalori").getValue()));
                    jmlkalorimakanan = jmlkalorimakanan +  dbjumlahkalori;

                    double dbjumlahkarbohidrat = Double.parseDouble(String.valueOf(ds.child("karbohidrat").getValue()));
                    jmlkarbohidratmakanan = jmlkarbohidratmakanan +  dbjumlahkarbohidrat;

                    double dbjumlahprotein = Double.parseDouble(String.valueOf(ds.child("protein").getValue()));
                    jmlproteinmakanan = jmlproteinmakanan +  dbjumlahprotein;

                    double dbjumlahlemak = Double.parseDouble(String.valueOf(ds.child("lemak").getValue()));
                    jmllemakmakanan = jmllemakmakanan +  dbjumlahlemak;

                }
                textkalori.setText(String.format(Locale.US,"%.2f", jmlkalorimakanan));
                textkarbohidrat.setText(String.format(Locale.US,"%.2f", jmlkarbohidratmakanan));
                textprotein.setText(String.format(Locale.US,"%.2f", jmlproteinmakanan));
                textlemak.setText(String.format(Locale.US,"%.2f", jmllemakmakanan));

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }

        });


        //refresh
        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(jmlkalorimakanan <= 0 && jmlkarbohidratmakanan <=0 && jmlproteinmakanan <=0 && jmllemakmakanan <=0 ){
                    hasilkalorimakanan.setBackgroundResource(R.drawable.loading);
                    Toast toast = Toast.makeText(HitungAct.this,
                            "Data Kosong", Toast.LENGTH_SHORT);
                    toast.show();
                }


                //hitung kalori makanan
                minbataskalori = jmlkebkalori - 300;
                maxbataskalori = jmlkebkalori + 300;

                if(jmlkalorimakanan <= 0 ){
                    hasilkalorimakanan.setBackgroundResource(R.drawable.loading);

                }
                else if(jmlkalorimakanan > minbataskalori && jmlkalorimakanan < maxbataskalori){
                    hasilkalorimakanan.setBackgroundResource(R.drawable.hasil_cukup);
                }
                else if(jmlkalorimakanan < minbataskalori ){
                    hasilkalorimakanan.setBackgroundResource(R.drawable.hasil_kurang);
                }
                else if(jmlkalorimakanan > maxbataskalori){
                    hasilkalorimakanan.setBackgroundResource(R.drawable.hasil_lebih);
                }

                //hitung karbohidrat makanan
                minbataskarbo = (jmlkebkalori * karbohidrat1)/4;
                maxbataskarbo = (jmlkebkalori * karbohidrat2)/4;

                if(jmlkarbohidratmakanan <=0 ){
                    hasilkarbohidratmakanan.setBackgroundResource(R.drawable.loading);

                }
                else if(jmlkarbohidratmakanan > minbataskarbo && jmlkarbohidratmakanan < maxbataskarbo){
                    hasilkarbohidratmakanan.setBackgroundResource(R.drawable.hasil_cukup);
                }
                else if(jmlkarbohidratmakanan < minbataskarbo ){
                    hasilkarbohidratmakanan.setBackgroundResource(R.drawable.hasil_kurang);
                }
                else if(jmlkarbohidratmakanan > maxbataskarbo){
                    hasilkarbohidratmakanan.setBackgroundResource(R.drawable.hasil_lebih);
                }

                //hitungprotein makanan
                minbatasprotein = (jmlkebkalori * protein1)/4;
                maxbatasprotein = (jmlkebkalori * protein2)/4;

                if(jmlproteinmakanan <=0 ){
                    hasilproteinmakanan.setBackgroundResource(R.drawable.loading);
                }
                else if(jmlproteinmakanan > minbatasprotein && jmlproteinmakanan < maxbatasprotein){
                    hasilproteinmakanan.setBackgroundResource(R.drawable.hasil_cukup);
                }
                else if(jmlproteinmakanan < minbatasprotein ){
                    hasilproteinmakanan.setBackgroundResource(R.drawable.hasil_kurang);
                }
                else if(jmlproteinmakanan > maxbatasprotein){
                    hasilproteinmakanan.setBackgroundResource(R.drawable.hasil_lebih);
                }

                //hitung lemak makanan
                minbataslemak = (jmlkebkalori * lemak1)/9;
                maxbataslemak = (jmlkebkalori * lemak2)/9;

                if(jmllemakmakanan <=0 ){
                    hasillemakmakanan.setBackgroundResource(R.drawable.loading);
                }
                else if(jmllemakmakanan > minbataslemak && jmllemakmakanan < maxbataslemak){
                    hasillemakmakanan.setBackgroundResource(R.drawable.hasil_cukup);
                }
                else if(jmllemakmakanan < minbataslemak ){
                    hasillemakmakanan.setBackgroundResource(R.drawable.hasil_kurang);
                }
                else if(jmllemakmakanan > maxbataslemak){
                    hasillemakmakanan.setBackgroundResource(R.drawable.hasil_lebih);
                }
            }
        });




// Pindah acivity Bottom Navigation
        btn_profilunclick = findViewById(R.id.btn_profilunclick);
        btn_profilunclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(HitungAct.this,ProfilAct.class);
                startActivity(go);
            }
        });

        btn_makanunclick = findViewById(R.id.btn_makanunclick );
        btn_makanunclick .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(HitungAct.this,PilihMakananAct.class);
                startActivity(go);
            }
        });

        btn_infounclick = findViewById(R.id.btn_infounclick);
        btn_infounclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(HitungAct.this,InfoGiziAct.class);
                startActivity(go);
            }
        });

        btn_tentangunclick = findViewById(R.id.btn_tentangunclick);
        btn_tentangunclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(HitungAct.this,TentangAct.class);
                startActivity(go);
            }
        });
    }
    public void getUsernameLocal(){
        SharedPreferences sharedPreferences = getSharedPreferences(USERNAME_KEY, MODE_PRIVATE);
        username_key_new =sharedPreferences.getString(username_key, "");

    }
}
