package com.l200150089.creiva.zagagizi;

import java.io.Serializable;

public class AInfoGizi implements Serializable {

    String judul_info, isi_info, key;

    public AInfoGizi() {
    }

    public AInfoGizi(String judul_info, String key) {
        this.judul_info = judul_info;
        this.key = key;
    }

    public String getIsi_info() {
        return isi_info;
    }

    public void setIsi_info(String isi_info) {
        this.isi_info = isi_info;
    }

    public String getJudul_info() {
        return judul_info;
    }

    public void setJudul_info(String judul_info) {
        this.judul_info = judul_info;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
