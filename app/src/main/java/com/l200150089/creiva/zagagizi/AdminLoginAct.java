package com.l200150089.creiva.zagagizi;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class AdminLoginAct extends AppCompatActivity {

    //firebase
    EditText xusername, xpassword;

    DatabaseReference reference;

    String USERNAME_KEY = "usernamekey";
    String username_key = "";


    Button btn_sign_in;
    ImageView logadmin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_login);

        xusername = findViewById(R.id.xusername);
        xpassword = findViewById(R.id.xpassword);

        btn_sign_in = findViewById(R.id.btn_sign_in);
        logadmin = findViewById(R.id.logadmin);

        xusername.addTextChangedListener(loginTextWatcher);
        xpassword.addTextChangedListener(loginTextWatcher);

        logadmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(AdminLoginAct.this,SignInAct.class);
                startActivity(go);


            }
        });

        btn_sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //loading
                btn_sign_in.setEnabled(false);
                btn_sign_in.setText("Tunggu...");


                //database
                String username = xusername.getText().toString();
                final String password = xpassword.getText().toString();

                reference= FirebaseDatabase.getInstance().getReference().
                        child("Admin").child(username);

                reference.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if(dataSnapshot.exists()){

                            // ambil data password dari firbase
                            String passwordFromFirebase = dataSnapshot.child("password").getValue().toString();

                            //Validasi password dengan firebase
                            if(password.equals(passwordFromFirebase)){
                                //loading
                                btn_sign_in.setEnabled(false);
                                btn_sign_in.setText("Tunggu...");

                                //Simpan username key pada local
                                SharedPreferences sharedPreferences = getSharedPreferences(USERNAME_KEY, MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString(username_key, xusername.getText().toString());
                                editor.apply();

                                //pindah activity
                                Intent gotoprofil = new Intent(
                                        AdminLoginAct.this,AdminMenu2.class);
                                startActivity(gotoprofil);


                            }
                            else {
                                Toast.makeText(getApplicationContext(),"Password salah!", Toast.LENGTH_SHORT).show();
                                //loading
                                btn_sign_in.setEnabled(true);
                                btn_sign_in.setText("Masuk");
                            }

                        }
                        else{
                            Toast.makeText(getApplicationContext(),"Username tidak ada!", Toast.LENGTH_SHORT).show();
                            //loading
                            btn_sign_in.setEnabled(true);
                            btn_sign_in.setText("Masuk");
                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


            }
        });
    }

    private TextWatcher loginTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String usernameInput = xusername.getText().toString().trim();
            String passwordInput = xpassword.getText().toString().trim();

            btn_sign_in.setEnabled(!usernameInput.isEmpty() && !passwordInput.isEmpty());


        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };
}
