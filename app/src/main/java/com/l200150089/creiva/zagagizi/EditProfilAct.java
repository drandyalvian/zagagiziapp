package com.l200150089.creiva.zagagizi;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class EditProfilAct extends AppCompatActivity {

    //firebase
    EditText xpassword,xnama_lengkap,xtinggi_badan, xberat_badan;
    Spinner xspinner,xspinner2;
    TextView xusia;
    SeekBar seekbarusia;
    Button infoaktivitas;

    DatabaseReference reference;
    String USERNAME_KEY = "usernamekey";
    String username_key = "";
    String username_key_new ="";

    Button btn_simpan;
    LinearLayout btn_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profil);

        getUsernameLocal();
        xnama_lengkap = findViewById(R.id.xnama_lengkap);
        xpassword = findViewById(R.id.xpassword);
//        xusername = findViewById(R.id.xusername);
        xtinggi_badan = findViewById(R.id.xtinggi_badan);
        xberat_badan = findViewById(R.id.xberat_badan);
        xusia = findViewById(R.id.xusia);
        seekbarusia=findViewById(R.id.seekbarusia);

        xnama_lengkap.addTextChangedListener(loginTextWatcher);
        xtinggi_badan.addTextChangedListener(loginTextWatcher);
        xberat_badan.addTextChangedListener(loginTextWatcher);
//        xusername.addTextChangedListener(loginTextWatcher);
        xpassword.addTextChangedListener(loginTextWatcher);

        xspinner = findViewById(R.id.xspinner);
        xspinner2 = findViewById(R.id.xspinner2);

        final ArrayAdapter pilihGender=ArrayAdapter.createFromResource(this, R.array.pilih_gender, android.R.layout.simple_spinner_dropdown_item);
        xspinner.setAdapter(pilihGender);
        final ArrayAdapter pilihAktivitas=ArrayAdapter.createFromResource(this, R.array.pilih_aktivitas, android.R.layout.simple_spinner_dropdown_item);
        xspinner2.setAdapter(pilihAktivitas);

        seekbarusia.setMax(51);
        seekbarusia.setProgress(0);
        seekbarusia.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progress = progress + 19;
                xusia.setText(String.valueOf(progress));

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        //mengambil data
        reference= FirebaseDatabase.getInstance().getReference().
                child("Users").child(username_key_new);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                xusername.setText(dataSnapshot.child("username").getValue().toString());
                xpassword.setText(dataSnapshot.child("password").getValue().toString());
                xnama_lengkap.setText(dataSnapshot.child("nama_lengkap").getValue().toString());
                xtinggi_badan.setText(dataSnapshot.child("tinggi_badan").getValue().toString());
                xberat_badan.setText(dataSnapshot.child("berat_badan").getValue().toString());
                xusia.setText(dataSnapshot.child("usia").getValue().toString());
                xspinner.setSelection(pilihGender.getPosition(dataSnapshot.child("jenis_kelamin").getValue().toString()));
                xspinner2.setSelection(pilihAktivitas.getPosition(dataSnapshot.child("aktivitas").getValue().toString()));

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        //klik button simpan
        btn_simpan = findViewById(R.id.btn_simpan);
        btn_simpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //database update
                reference.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                        dataSnapshot.getRef().child("username").setValue(xusername.getText().toString());
                        dataSnapshot.getRef().child("password").setValue(xpassword.getText().toString());
                        dataSnapshot.getRef().child("nama_lengkap").setValue(xnama_lengkap.getText().toString());
                        dataSnapshot.getRef().child("tinggi_badan").setValue(xtinggi_badan.getText().toString());
                        dataSnapshot.getRef().child("berat_badan").setValue(xberat_badan.getText().toString());
                        dataSnapshot.getRef().child("usia").setValue(xusia.getText().toString());
                        dataSnapshot.getRef().child("jenis_kelamin").setValue(xspinner.getSelectedItem().toString());
                        dataSnapshot.getRef().child("aktivitas").setValue(xspinner2.getSelectedItem().toString());


                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

                //pindah aactivity seeprofil
                Intent goseeprofil = new Intent(EditProfilAct.this,SeeProfilAct.class);
                startActivity(goseeprofil);
            }
        });

        infoaktivitas = findViewById(R.id.infoaktivitas);
        infoaktivitas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dialog dialog = new Dialog(EditProfilAct.this);
                dialog.setContentView(R.layout.dialog_aktivitas);
                dialog.setTitle("Aktivitas");
                dialog.show();
            }
        });

        btn_back = findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goseeprofil = new Intent(EditProfilAct.this,SeeProfilAct.class);
                startActivity(goseeprofil);
            }
        });
    }

    public void getUsernameLocal(){
        SharedPreferences sharedPreferences = getSharedPreferences(USERNAME_KEY, MODE_PRIVATE);
        username_key_new =sharedPreferences.getString(username_key, "");

    }

    private TextWatcher loginTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String tbInput = xtinggi_badan.getText().toString().trim();
            String bbInput = xberat_badan.getText().toString().trim();
            String namaInput = xnama_lengkap.getText().toString().trim();
//            String usernameInput = xusername.getText().toString().trim();
            String passwordInput = xpassword.getText().toString().trim();

            btn_simpan.setEnabled(!tbInput.isEmpty() && !bbInput.isEmpty() && !namaInput.isEmpty()
                    && !passwordInput.isEmpty());





        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };
}
