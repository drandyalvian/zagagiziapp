package com.l200150089.creiva.zagagizi;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class AdminAddInfoGiziAct extends AppCompatActivity {

    EditText judul_info, isi_info;
    Button btn_tambah;
    LinearLayout btn_back;
    DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_add_info_gizi);

        judul_info = findViewById(R.id.judul_info);
        isi_info = findViewById(R.id.isi_info);

        btn_back = findViewById(R.id.btn_back);
        btn_tambah = findViewById(R.id.btn_tambah);

        btn_tambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //loading
                btn_tambah.setEnabled(false);
                btn_tambah.setText("Tunggu...");

                //Simpan Database
                reference = FirebaseDatabase.getInstance().getReference()
                        .child("InfoGizis").child(judul_info.getText().toString());
                reference.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        dataSnapshot.getRef().child("judul_info").setValue(judul_info.getText().toString());
                        dataSnapshot.getRef().child("isi_info").setValue(isi_info.getText().toString());
                        dataSnapshot.getRef().child("key").setValue(judul_info.getText().toString());
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
                //pindah activity
                Intent go=new Intent(AdminAddInfoGiziAct.this,AdminInfoGiziAct.class);
                startActivity(go);


            }
        });

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent go=new Intent(AdminAddInfoGiziAct.this,AdminInfoGiziAct.class);
                startActivity(go);
            }
        });
    }
}
