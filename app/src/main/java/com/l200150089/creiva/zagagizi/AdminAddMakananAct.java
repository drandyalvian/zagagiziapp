package com.l200150089.creiva.zagagizi;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Locale;

public class AdminAddMakananAct extends AppCompatActivity {

    //firebase
    EditText nama_makanan, jenis_makanan, gram, porsi, kalori, karbohidrat, protein, lemak;
    Button btn_tambah;
    LinearLayout btn_back;
    DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_add_makanan);

        nama_makanan = findViewById(R.id.nama_makanan);
        jenis_makanan = findViewById(R.id.jenis_makanan);
        porsi = findViewById(R.id.porsi);
        gram = findViewById(R.id.gram);
        kalori = findViewById(R.id.kalori);
        karbohidrat = findViewById(R.id.karbohidrat);
        protein = findViewById(R.id.protein);
        lemak = findViewById(R.id.lemak);

        btn_back = findViewById(R.id.btn_back);
        btn_tambah = findViewById(R.id.btn_tambah);

        btn_tambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //loading
                btn_tambah.setEnabled(false);
                btn_tambah.setText("Tunggu...");

                //Simpan Database
                reference = FirebaseDatabase.getInstance().getReference()
                        .child("DataMakanan").child(nama_makanan.getText().toString());
                reference.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        dataSnapshot.getRef().child("nama_makanan").setValue(nama_makanan.getText().toString());
                        dataSnapshot.getRef().child("jenis_makanan").setValue(jenis_makanan.getText().toString());
                        dataSnapshot.getRef().child("gram").setValue(gram.getText().toString());
                        dataSnapshot.getRef().child("porsi").setValue(porsi.getText().toString());
                        dataSnapshot.getRef().child("kalori").setValue(kalori.getText().toString());
                        dataSnapshot.getRef().child("karbohidrat").setValue(karbohidrat.getText().toString());
                        dataSnapshot.getRef().child("protein").setValue(protein.getText().toString());
                        dataSnapshot.getRef().child("lemak").setValue(lemak.getText().toString());
                        dataSnapshot.getRef().child("key").setValue(nama_makanan.getText().toString());

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
                //pindah activity
                Intent go=new Intent(AdminAddMakananAct.this,AdminDataMakananAct.class);
                startActivity(go);


            }
        });
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent go=new Intent(AdminAddMakananAct.this,AdminDataMakananAct.class);
                startActivity(go);
            }
        });
    }
}
