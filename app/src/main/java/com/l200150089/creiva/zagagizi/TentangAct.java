package com.l200150089.creiva.zagagizi;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Locale;

public class TentangAct extends AppCompatActivity {

    Button btn_profilunclick;
    Button btn_makanunclick;
    Button btn_infounclick;

    TextView isi_tentang;

    DatabaseReference reference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tentang);

        isi_tentang = findViewById(R.id.isi_tentang);

        //        Mengambil data dari firebase
        reference = FirebaseDatabase.getInstance().getReference().child("Tentang").child("Tentang Aplikasi");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                isi_tentang.setText(dataSnapshot.child("isi_tentang").getValue().toString());

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


//Pindah activity bottom navigation
        btn_profilunclick=findViewById(R.id.btn_profilunclick);
        btn_profilunclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(TentangAct.this,ProfilAct.class);
                startActivity(go);
            }
        });

        btn_makanunclick=findViewById(R.id.btn_makanunclick);
        btn_makanunclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(TentangAct.this,PilihMakananAct.class);
                startActivity(go);
            }
        });

        btn_infounclick=findViewById(R.id.btn_infounclick);
        btn_infounclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(TentangAct.this,InfoGiziAct.class);
                startActivity(go);
            }
        });



    }
}
