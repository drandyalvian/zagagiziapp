package com.l200150089.creiva.zagagizi;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class ADataMakananAdapter extends RecyclerView.Adapter<ADataMakananAdapter.MyViewHolder> {

    FirebaseDataListener listener;

    Context context;
    ArrayList<ADataMakanan> adataMakanan;
    public ADataMakananAdapter(Context c, ArrayList<ADataMakanan> p, Context b ){
        context = c;
        adataMakanan = p;

        listener = (AdminDataMakananAct) b;

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public ADataMakananAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_admin_datamakanan, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, final int  i) {


        final String gnamamakanan = adataMakanan.get(i).getNama_makanan();
        final String gjenismakanan = adataMakanan.get(i).getJenis_makanan();

        System.out.println("DATA one by one "+i+adataMakanan.size());

        myViewHolder.xnama_makanan.setText(gnamamakanan);
        myViewHolder.xjenis_makanan.setText(gjenismakanan);


        //lihat data
        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(AdminDetailMakananAct.getActIntent((Activity) context)
                        .putExtra("data", adataMakanan.get(i)));
            }
        });

        myViewHolder.delbutton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        final Dialog dialog = new Dialog(context);
                        dialog.setContentView(R.layout.dialog_view);
                        dialog.setTitle("Pilih Aksi");
                        dialog.show();

                        Button delbtn = (Button) dialog.findViewById(R.id.dialogdelete);
                        Button editbtn = (Button) dialog.findViewById(R.id.dialogedit);

                        delbtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                listener.onDeleteData(adataMakanan.get(i), i);
                            }
                        });

                        editbtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                context.startActivity(AdminEditMakananAct.getActIntent((Activity) context)
                                        .putExtra("data", adataMakanan.get(i)));
                            }
                        });

                    }
                }
        );

    }

    @Override
    public int getItemCount() {
        return adataMakanan.size();
    }




    class MyViewHolder extends RecyclerView.ViewHolder{

        TextView xnama_makanan,  xjenis_makanan;
        ImageView delbutton;


        public MyViewHolder(@NonNull View itemView){
            super(itemView);

            xnama_makanan = itemView.findViewById(R.id.xnama_makanan);
            xjenis_makanan = itemView.findViewById(R.id.xjenis_makanan);

            delbutton = itemView.findViewById(R.id.delbutton);
        }
    }
    public interface FirebaseDataListener{
        void onDeleteData(ADataMakanan adataMakanan, int i);
    }
}
