package com.l200150089.creiva.zagagizi;

public class ADataUser {

    String nama_lengkap, username, key;

    public ADataUser() {
    }

    public ADataUser(String nama_lengkap, String username, String key) {
        this.nama_lengkap = nama_lengkap;
        this.username = username;
        this.key = key;
    }

    public String getNama_lengkap() {
        return nama_lengkap;
    }

    public void setNama_lengkap(String nama_lengkap) {
        this.nama_lengkap = nama_lengkap;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
