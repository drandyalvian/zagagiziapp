package com.l200150089.creiva.zagagizi;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class InfoGiziAct extends AppCompatActivity {

    Button btn_profilunclick;
    Button btn_makanuclick;
    Button btn_tentangunclick;

    DatabaseReference reference;

//    String USERNAME_KEY = "usernamekey";
//    String username_key ="";
    //String username_key_new ="";

    RecyclerView infogizi_place;
    ArrayList<InfoGizi> list2;
    InfoGiziAdapter infoGiziAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_gizi);

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.NetworkInfo wifi = cm
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        android.net.NetworkInfo datac = cm
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if((wifi != null & datac != null) && (wifi.isConnected()| datac.isConnected())){

        }else {
            Toast toast = Toast.makeText(InfoGiziAct.this,
                    "Tidak ada koneksi", Toast.LENGTH_SHORT);
            toast.show();
        }

        //getUsernameLocal();
        infogizi_place=findViewById(R.id.infogizi_place);
        infogizi_place.setLayoutManager(new LinearLayoutManager(this));
        list2 = new ArrayList<InfoGizi>();

        reference = FirebaseDatabase.getInstance()
                .getReference().child("InfoGizis");
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot dataSnapshot1: dataSnapshot.getChildren()){
                    InfoGizi p =dataSnapshot1.getValue(InfoGizi.class);
                    list2.add(p);

                    infoGiziAdapter = new InfoGiziAdapter(InfoGiziAct.this, list2);
                    infogizi_place.setAdapter(infoGiziAdapter);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


//Pindah activity bottom navigation
        btn_profilunclick=findViewById(R.id.btn_profilunclick);
        btn_profilunclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(InfoGiziAct.this,ProfilAct.class);
                startActivity(go);
            }
        });


        btn_makanuclick=findViewById(R.id.btn_makanuclick);
        btn_makanuclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(InfoGiziAct.this,PilihMakananAct.class);
                startActivity(go);
            }
        });

        btn_tentangunclick=findViewById(R.id.btn_tentangunclick);
        btn_tentangunclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go = new Intent(InfoGiziAct.this,TentangAct.class);
                startActivity(go);
            }
        });


    }

//    public void getUsernameLocal(){
//        SharedPreferences sharedPreferences = getSharedPreferences(USERNAME_KEY, MODE_PRIVATE);
//        username_key_new =sharedPreferences.getString(username_key, "");
//
//    }



}
