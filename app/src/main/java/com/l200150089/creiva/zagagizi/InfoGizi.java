package com.l200150089.creiva.zagagizi;

public class InfoGizi {
    String judul_info, key;

    public InfoGizi() {
    }

    public InfoGizi(String judul_info, String key) {
        this.judul_info = judul_info;
        this.key = key;
    }

    public String getJudul_info() {
        return judul_info;
    }

    public void setJudul_info(String judul_info) {
        this.judul_info = judul_info;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
