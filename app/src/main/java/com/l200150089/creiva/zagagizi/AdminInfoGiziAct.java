package com.l200150089.creiva.zagagizi;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class AdminInfoGiziAct extends AppCompatActivity implements AInfoGiziAdapter.FirebaseDataListener {

    DatabaseReference reference, reference2;
    private RecyclerView rvView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<AInfoGizi> list2;

    EditText txtsearch;
    Button addinfo;
    Button btn_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_info_gizi);

        EditText editor = new EditText(this);
        editor.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS);

        txtsearch = findViewById(R.id.txtsearch);
        addinfo = findViewById(R.id.addinfo);

        rvView = (RecyclerView) findViewById(R.id.infogizi_place);
        rvView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        rvView.setLayoutManager(layoutManager);

        reference2 = FirebaseDatabase.getInstance()
                .getReference().child("InfoGizis");

        reference = FirebaseDatabase.getInstance().getReference();
        reference.child("InfoGizis").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                list2 = new ArrayList<>();
                for (DataSnapshot dataSnapshot1: dataSnapshot.getChildren()){

                    AInfoGizi ainfogizi = dataSnapshot1.getValue(AInfoGizi.class);
                    ainfogizi.setKey(dataSnapshot1.getKey());

                    list2.add(ainfogizi);
                }
                adapter = new AInfoGiziAdapter( AdminInfoGiziAct.this, list2, AdminInfoGiziAct.this);
                rvView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        txtsearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().isEmpty()){
                    search(s.toString());
                }else {
                    search("");
                }


            }
        });

        //pindah activity
        addinfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go=new Intent(AdminInfoGiziAct.this,AdminAddInfoGiziAct.class);
                startActivity(go);
            }
        });

        btn_back = findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goseeprofil = new Intent(AdminInfoGiziAct.this,AdminMenu2.class);
                startActivity(goseeprofil);
            }
        });
    }

    public static Intent getActIntent(Activity activity){
        return new Intent(activity, AdminInfoGiziAct.class);
    }

    public void onDeleteData(AInfoGizi ainfoGizi, final int i){
        if(reference!=null){
            reference.child("InfoGizis").child(ainfoGizi.getKey()).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Toast.makeText(AdminInfoGiziAct.this,"success delete", Toast.LENGTH_LONG).show();
                }
            });

        }
    }

    private void search(String s) {

        Query query = reference2.orderByChild("judul_info")
                .startAt(s)
                .endAt(s+"\uf8ff");
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChildren()){
                    list2.clear();
                    for (DataSnapshot dss: dataSnapshot.getChildren()){
                        final AInfoGizi ainfoGizi = dss.getValue(AInfoGizi.class);
                        list2.add(ainfoGizi);
                    }

                    AInfoGiziAdapter adapter = new AInfoGiziAdapter(AdminInfoGiziAct.this,list2, AdminInfoGiziAct.this);
                    rvView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
