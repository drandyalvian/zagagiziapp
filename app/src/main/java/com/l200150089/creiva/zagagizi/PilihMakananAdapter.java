package com.l200150089.creiva.zagagizi;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class PilihMakananAdapter extends RecyclerView.Adapter<PilihMakananAdapter.MyViewHolder> {

   FirebaseDataListener listener;

    Context context;
    ArrayList<PilihMakanan> pilihMakanan;
    public PilihMakananAdapter(ArrayList<PilihMakanan> p,Context c ){
        context = c;
        pilihMakanan = p;
        listener = (PilihMakananAct) c;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pilih_makanan, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, final int i) {


        final String gnamamakanan = pilihMakanan.get(i).getNama_makanan();
        final String gjenismakanan = pilihMakanan.get(i).getJenis_makanan();
        final String gporsi = pilihMakanan.get(i).getPorsi();
        final String ggram = pilihMakanan.get(i).getGram();
        final String gkalori = pilihMakanan.get(i).getKalori();
        final String gkarbo = pilihMakanan.get(i).getKarbohidrat();
        final String gprotein = pilihMakanan.get(i).getProtein();
        final String glemak = pilihMakanan.get(i).getLemak();

        myViewHolder.xnama_makanan.setText(gnamamakanan);
        myViewHolder.xjenis_makanan.setText(gjenismakanan);
        myViewHolder.xporsi.setText(gporsi+" porsi");
        myViewHolder.xgram.setText(ggram+" gram");
        myViewHolder.xkalori.setText(gkalori+" kkal");
        myViewHolder.xkarbohidrat.setText(gkarbo+" gram");
        myViewHolder.xprotein.setText(gprotein+" gram");
        myViewHolder.xlemak.setText(glemak+" gram");



        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goToDetailMakanan = new Intent(context, DetailMakanankuAct.class);
                goToDetailMakanan.putExtra("nama_makanan",gnamamakanan);
                context.startActivity(goToDetailMakanan);
            }
        });

        myViewHolder.delbutton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listener.onDeleteData(pilihMakanan.get(i), i);
                    }
                }
        );




    }

    @Override
    public int getItemCount() {
        return pilihMakanan.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        TextView xnama_makanan,  xjenis_makanan, xporsi, xgram, xkalori, xkarbohidrat, xprotein, xlemak;
        ImageView delbutton;


        public MyViewHolder(@NonNull View itemView){
            super(itemView);



            xnama_makanan = itemView.findViewById(R.id.xnama_makanan);
            xjenis_makanan = itemView.findViewById(R.id.xjenis_makanan);
            xporsi = itemView.findViewById(R.id.xporsi);
            xgram = itemView.findViewById(R.id.xgram);
            xkalori = itemView.findViewById(R.id.xkalori);
            xkarbohidrat = itemView.findViewById(R.id.xkarbohidrat);
            xprotein = itemView.findViewById(R.id.xprotein);
            xlemak = itemView.findViewById(R.id.xlemak);

            delbutton = itemView.findViewById(R.id.delbutton);
        }

    }
    public interface FirebaseDataListener{
        void onDeleteData(PilihMakanan pilihMakanan, int i);
    }




}

