package com.l200150089.creiva.zagagizi;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Locale;

public class AdminDetailMakananAct extends AppCompatActivity {

    TextView textjumlahgram, textjumlahporsi, textjumlahkalori,textjumlahkarbohidrat,
            textjumlahprotein, textjumlahlemak, xnama_makanan, xjenis_makanan;

    DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_detail_makanan);

        xnama_makanan = findViewById(R.id.xnama_makanan);
        xjenis_makanan = findViewById(R.id.xjenis_makanan);
        textjumlahgram = findViewById(R.id.textjumlahgram);
        textjumlahporsi = findViewById(R.id.textjumlahporsi);
        textjumlahkalori = findViewById(R.id.textjumlahkalori);
        textjumlahkarbohidrat = findViewById(R.id.textjumlahkarbohidrat);
        textjumlahprotein= findViewById(R.id.textjumlahprotein);
        textjumlahlemak = findViewById(R.id.textjumlahlemak);

        ADataMakanan adataMakanan = (ADataMakanan) getIntent().getSerializableExtra("data");
        if(adataMakanan!=null){
            xnama_makanan.setText(adataMakanan.getNama_makanan());
            xjenis_makanan.setText(adataMakanan.getJenis_makanan());
            textjumlahkalori.setText(adataMakanan.getKalori());
            textjumlahgram.setText(adataMakanan.getGram());
            textjumlahporsi.setText(adataMakanan.getPorsi());
            textjumlahkarbohidrat.setText(adataMakanan.getKarbohidrat());
            textjumlahprotein.setText(adataMakanan.getProtein());
            textjumlahlemak.setText(adataMakanan.getLemak());
        }

//        //      mengambil data dari intent
//        Bundle bundle = getIntent().getExtras();
//        final String nama_makanan_baru = bundle.getString("nama_makanan");
//
////      Mengambil data dari firebase
//        reference = FirebaseDatabase.getInstance().getReference().child("DataMakanan").child(nama_makanan_baru);
//        reference.addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//
//                xnama_makanan.setText(dataSnapshot.child("nama_makanan").getValue().toString());
//                xjenis_makanan.setText(dataSnapshot.child("jenis_makanan").getValue().toString());
//                textjumlahgram.setText(dataSnapshot.child("gram").getValue().toString());
//                textjumlahporsi.setText(dataSnapshot.child("porsi").getValue().toString());
//
//                double dbjumlahkalori = Double.valueOf(dataSnapshot.child("kalori").getValue().toString());
//                textjumlahkalori.setText(String.format(Locale.US,"%.2f", dbjumlahkalori));
//                double dbjumlahkarbohidrat = Double.valueOf(dataSnapshot.child("karbohidrat").getValue().toString());
//                textjumlahkarbohidrat.setText(String.format(Locale.US,"%.2f", dbjumlahkarbohidrat));
//                double dbjumlahprotein = Double.valueOf(dataSnapshot.child("protein").getValue().toString());
//                textjumlahprotein.setText(String.format(Locale.US,"%.2f", dbjumlahprotein));
//                double dbjumlahlemak = Double.valueOf(dataSnapshot.child("lemak").getValue().toString());
//                textjumlahlemak.setText(String.format(Locale.US,"%.2f", dbjumlahlemak));
//
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });


    }
    public static Intent getActIntent(Activity activity){
        return new Intent(activity, AdminDetailMakananAct.class);
    }
}
